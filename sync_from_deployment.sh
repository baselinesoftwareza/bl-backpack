#!/bin/bash

# sync from a local deployment which you have been modifying
# check if there is 1 argument
if [ -z "$1" ]; then
    echo "Usage: $0 <deployment_path> [dry-run]"
    echo "eg   : $0 ../deployment/project/path"
    exit 1
fi

DRY_RUN=""
if [ ! -z "$2" ]; then
    echo "Dry run"
    DRY_RUN="--dry-run"
fi

DEPLOYMENT_PATH="$1/vendor/baseline/bl-backpack"

# check if the deployment path exists
if [ ! -d "$DEPLOYMENT_PATH" ]; then
    echo "Deployment path $DEPLOYMENT_PATH does not exist"
    exit 1
fi

# sync the files
rsync -av --ignore-times --checksum --delete --exclude .git --exclude sync_from_deployment.sh $DRY_RUN $DEPLOYMENT_PATH/ ./
