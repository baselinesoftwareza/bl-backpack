{!! '<?php' !!}

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class {{ $class }} extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('{{ $table }}', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('{{ $tenant['foreign_key'] }}');
@foreach($ancestors ?? [] as $ancestor)
            $table->bigInteger('{{ $ancestor }}_id');
@endforeach
@foreach($fields ?? [] as $field)
    @if($field == 'enabled')
            $table->boolean('{{ $field }}')->default(true);
    @elseif($field == 'basename')
            $table->string('{{ $field }}');
    @elseif($field == 'location')
            // location is special case, converts to address, longitude, latitude
            $table->text('address');
            $table->string('longitude');
            $table->string('latitude');
    @else
            $table->string('{{ $field }}')->nullable();
   @endif
@endforeach
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('{{ $table }}');
    }
}
