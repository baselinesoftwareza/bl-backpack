{!! '<?php' !!}

namespace App\Http\Requests\Admin;

use Baseline\Backpack\Helpers\RouteHelper;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class {{ $model }}Request extends \Illuminate\Foundation\Http\FormRequest
{
    public function authorize()
    {
        return backpack_auth()->check();
    }

    public function rules(){
        $rules = [];
        $this->addCommonRules($rules);
        if(RouteHelper::is_create()) {
            $this->addCreateRules($rules);
        }
        if(RouteHelper::is_update()) {
            $this->addUpdateRules($rules);
        }
        return $rules;
    }

    public function addCommonRules(&$rules) {
@foreach($fields ?? [] as $field)
@if($field == 'enabled')
        $rules['{{ $field }}'] = 'required|boolean';
@elseif($field == 'basename')
@elseif(Str::contains(strtolower($field),'phone'))
        $rules['{{ $field }}'] = 'required|phone:AUTO,'.implode(',',config('bl-backpack.validation.numbers_supported',[])).'|max:255';
@elseif(Str::contains(strtolower($field),'mail'))
        $rules['{{ $field }}'] = 'required|email:rfc,dns';
@elseif(Str::contains(strtolower($field),'location'))
        $rules['location'] = 'required|json';
@else
        $rules['{{ $field }}'] = 'required|max:255';
@endif
@endforeach
    }

    public function addCreateRules(&$rules) {
@if(in_array('basename',$fields ?? []))
        $rules['basename'] = ['required','min:2','max:100','valid_basename',Rule::unique('{{ $table }}')->where(fn ($query) =>
@if($route_parent ?? null)
            $query->where('{{ $route_parent }}_id', request()->input('{{ $route_parent }}_id'))
@else
            $query->where('partner_id', PartnerHelper::currentPartnerId())
@endif
        //
        )];
@endif
    }

    public function addUpdateRules(&$rules) {

    }

    public function messages()
    {
        return [
@foreach($fields ?? [] as $field)
    @if(Str::contains(strtolower($field),'phone'))
        '{{ $field }}.phone' => 'Please enter a valid phone number',
    @endif
@endforeach
        ];
    }
}
