{!! '<?php' !!}

namespace App\Models;

use Baseline\Backpack\Model\Traits\ProtectAttribute;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Baseline\Backpack\Model\Traits\IsTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
@if(in_array('location',$fields ?? []))
use Baseline\Backpack\Model\Traits\LocationTrait;
@endif

class {{ $model }} extends Model
{
    use CrudTrait,
        Notifiable,
        ProtectAttribute,
        @if(in_array('location',$fields ?? []))
            LocationTrait,
        @endif
        IsTenant;

    protected $table = '{{ $table }}';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = [ "id" ];
@if($fields ?? null)
    // uncomment this to review the attribute which should be used to "name" the record in a human readable way
    //protected $indicatorAttribute = '{{ $fields[0] }}';
@endif
    protected $encryptable = [
@foreach($fields ?? [] as $field)
@if($field == 'password')
        '{{ $field }}',
@endif
@endforeach
    ];
    protected $fillable = [
@foreach($fields ?? [] as $field)
    @if($field == 'location')
        "location", // fake field
        "address",
        "longitude",
        "latitude",
    @else
        "{{ $field }}",
    @endif
@endforeach
@foreach($ancestors ?? [] as $ancestor)
        "{{ $ancestor }}_id",
@endforeach
    ];

    protected $noupdate = [
        '{{ $tenant['foreign_key'] }}',
@if(in_array('basename',$fields ?? []))
        'basename',
@endif
    ];

    protected static function boot()
    {
        parent::boot();
    }

    public function {{ $tenant['relation'] }}() {
        return $this->belongsTo('{{ $tenant['model'] }}');
    }

@foreach($ancestors ?? [] as $ancestor)
    public function {{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToCamelCase($ancestor) }}() {
        return $this->belongsTo('App\Models\{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToCamelCase($ancestor,true) }}');
    }
@endforeach
}
