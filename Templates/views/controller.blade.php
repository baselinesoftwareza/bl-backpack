{!! '<?php' !!}

namespace App\Http\Controllers\Admin;

@if(in_array('create',$operations) || in_array('update',$operations))
use App\Http\Requests\Admin\{{ $model }}Request;
@endif
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\FilterHelper;
use Baseline\Backpack\Operations\CreateOperation;
use Baseline\Backpack\Operations\DashboardOperation;
use Baseline\Backpack\Operations\DeleteOperation;
use Baseline\Backpack\Operations\ListOperation;
use Baseline\Backpack\Operations\SecretOperation;
use Baseline\Backpack\Operations\ToggleOperation;
use Baseline\Backpack\Operations\UpdateOperation;
use Baseline\Backpack\Operations\PopupOperation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class {{ $model }}Controller extends CrudController
{
    use BaselineCoreController;

@if(in_array('list',$operations))
    // For list/index (add { index as baselineIndex; } if required to create index)
    use ListOperation;
@endif
@if(in_array('dashboard',$operations))
    // for dashboard (you WILL need a dashboard method)
    use DashboardOperation;
@endif
@if(in_array('update',$operations))
    // for edit/update (add { update as baselineUpdate; } if required to create update)
    use UpdateOperation;
@endif
@if(in_array('create',$operations))
    // for create/store (add { store as baselineStore; } if required to create store)
    use CreateOperation;
@endif
@if(in_array('delete',$operations))
    // for deletion (add { destroy as baselineDestroy; } if required to create destroy)
    use DeleteOperation;
@endif
@if(in_array('toggle',$operations))
    // for the toggle function (add { toggle as baselineToggle; } if required to create toggle)
    use ToggleOperation;
@endif
@if(in_array('popup',$operations))
    // for the popup function (add { popup as baselinePopup; } if required to create popup)
    use PopupOperation;
@endif
@if(in_array('secret',$operations))
    // for the secret function (check below to set the attribute that holds the secret)
    use SecretOperation;
@endif

    public static $crud_config = [
        'model' => 'App\Models\{{ $model }}',
        'entity_name_strings' => ['{{ $name_singular }}', '{{ $name_plural }}'],
        'basename' => '{{ $basename }}',
        'icon' => 'la-cog',
        @if($route_parent ?? false)'parent' => '{{ $route_parent }}', // optional

        // The button menu items on parents
        'parent_index' => [
            'disabled' => false,
            'label' => '{{ \Illuminate\Support\Str::ucfirst($name_plural) }}',
            'type' => 'dropdown', // try 'inline'
            'priority' => '5', // what order to put them in
        ],
        // The main menu
        'menu' => [
            'group' => [
                'label' => '{{ \Illuminate\Support\Str::ucfirst($name_plural) }}',
            ],
            'section' => 'main',
            'priority' => '5',
            'menuitems' => [
                [
                    'operation' => 'list',
                    'label' => 'List {{ \Illuminate\Support\Str::ucfirst($name_plural) }}',
                    'icon' => 'la la-list'
                ],
@if(in_array('create',$operations))
                [
                    'operation' => 'create',
                    'label' => 'Add {{ \Illuminate\Support\Str::ucfirst($name_singular) }}',
                    'icon' => 'la la-plus'
                ]
@endif
            ],
        ],
@endif
        @if($baseroute ?? false)'baseroute' => '{{ $baseroute }}', // only for global baseroutes
@endif
    ];

    public function setup()
    {
        // Sets up the controller
        $this->setupController();
        // Any other setup thats not operation specific
    }

@if(in_array('list',$operations))
    protected function setupListOperation()
    {
        // Columns
@foreach($fields ?? [] as $field)
@if($field == 'enabled')
        $this->crud->column('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}')
            ->type('active_checkbox');
@else
        $this->crud->column('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}');
@endif
@endforeach
        //$this->crud->addColumn(['name' => 'field_name', 'label' => 'Nice Label', 'type' => 'text']);
        //$this->crud->addColumn(['name' => 'enabled', 'label' => 'Enabled', 'type' => 'active_checkbox']);
        // Filters
@if(in_array('enabled',$fields))
        FilterHelper::addEnabledFilter($this->crud,"Enabled","enabled");
@else
        // FilterHelper::addEnabledFilter($this->crud,"Enabled","enabled");
@endif
        // Top Buttons
        // $this->crud->addButton('top', 'back', 'view', 'vendor.backpack.crud.buttons.back_to_dashboard', 'beginning');
        // Add line buttons

        // set default order
        //if (!request()->has('order')) { $this->crud->orderBy('name'); }
    }
@endif


@if(in_array('popup',$operations))
    protected function setupPopupOperation()
    {
        /*  // This is for the id card layout
        $this->crud->column('avatar')->label('Avatar')
            ->type('image')->height('60px');
        $this->data['avatar'] = ['column' => 'avatar', 'height' => '100%', 'width' => '100%'];
        $this->crud->setOperationSetting('view','crud::popup.idcard');
        */

        // works the same as list
        // $this->crud->column('first_name')->label('First Name');
    }
@endif

@if(in_array('create',$operations))
    /**
     * This method preps the create options, setup validation, add your fields and other functionality prior to create / store method
     */
    protected function setupCreateOperation()
    {
        // Setup validation
        $this->crud->setValidation({{ $model }}Request::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Fields for create only
@if(in_array('basename',$fields))
        $this->crud->field('basename')->label('Basename')
            ->type('basename')->generate_from('name')->after('name')->wrapper(['class' => 'form-group col-md-6']);
@endif
        //$this->crud->field('field_name')->label('Nice Label')->type('text')->hint('Instruction Text');
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                {{ in_array('dashboard',$operations) ? '// ' : '' }}return $this->getReturnAddress($itemId) ?? $this->crud->route;
                // return to a dashboard
                {{ in_array('dashboard',$operations) ? '' : '// ' }}return $this->getReturnAddress($itemId) ?? $this->crud->route . "/" . $this->crud->entry->id . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }
@endif

@if(in_array('update',$operations))
    /**
     * This method allows you to setup the validation, fields and other stuff for edit/update
     */
    protected function setupUpdateOperation()
    {
        // Setup validation
        $this->crud->setValidation({{ $model }}Request::class);
        // Setup fields
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Update specific fields
@if(in_array('basename',$fields))
    $this->crud->field('basename')->label('Basename')
            ->after('name')->type('immutable')->wrapper(['class' => 'form-group col-md-6']);
@endif
        //$this->crud->field('field_name')->label('Nice Label')->type('text')->hint('Instruction Text');
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function($crud, $request, $itemId) {
                // return to the list
                {{ in_array('dashboard',$operations) ? '// ' : '' }}return $this->getReturnAddress($itemId) ?? $this->crud->route;
                // return to a dashboard
                {{ in_array('dashboard',$operations) ? '' : '// ' }}return $this->getReturnAddress($itemId) ?? $this->crud->route . "/" . $request->input('id') . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }
@endif

@if(in_array('create',$operations) || in_array('update',$operations))

    /**
     * This method allows you to setup common fields for update and create
     */
    private function setupCommonFields()
    {
@foreach($fields ?? [] as $field)
@if($field == 'enabled')
        $this->crud->field('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}')
            ->type('checkbox')->default(true);
@elseif($field == 'email')
        $this->crud->field('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}')
            ->type('email');
@elseif($field == 'password')
    $this->crud->field('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}')
        ->type('password');
@elseif($field == 'location')
    $entry = $this->crud->getCurrentEntry();
    // Check address, otherwise we get a nonsense location object
    $this->crud->field('location')->type("google_map")
        ->map_options([
        'height' => 600,
        'default_lng' => 18.42639,
        'default_lat' => -33.90743,
        'locate' => true,
    ])->value(($entry && $entry?->address) ? $entry->location : '');
@elseif($field == 'name' && in_array('basename',$fields))
        $this->crud->field('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}')
            ->wrapper(['class' => 'form-group col-md-6']);
@elseif($field == 'basename') {{-- No basename field, handled in setupC/U --}}
@else
        $this->crud->field('{{ $field }}')->label('{{ \Baseline\Backpack\Helpers\MiscHelper::underscoresToSpaceUcfirst($field,true) }}');
@endif
@endforeach
        //$this->crud->field('name')->label('Client Name')->hint('The name of the client or business name');
        //$this->crud->field('enabled')->label('Enabled')->hint('Is this enabled?')->default(true);
    }

    // If you have dependency fields, you can define them here and link them into create and update
/*
    private function getDependencyFields() {
        return [
            // this will get run as before any of the other changes (used to hide everything before something is shown)
            '_unset' => [
                'webhook_url' => ['disabled' => true, 'parent_visible' => false ],
                'msg_app_password' => ['disabled' => true, 'parent_visible' => false ],
            ],
            // field name or selector [name=applies_to]
            'applies_to' => [
                // switch/case values
                'site' => [
                    // Change this field or exec
                    'site_id' => ['disabled' => false, 'parent_visible' => true ],
                ],
                '*' => [
                    // Change this field or exec
                    'site_id' => ['disabled' => true, 'parent_visible' => false ],
                ],
            ],
            // field name
            'service_id' => [
                // switch/case values
                '*' => [
                    // Change this field or exec
                    'documentation' => [ 'function' => 'updateInfoFromTemplate($(this).val());' ],
                ],
            ],
        ];
    }
*/
@endif

@if(in_array('secret',$operations))
    /** IF you have a secretOperation
     * What attribute on the model holds the secret
     */
    protected function setupSecretOperation() {
        $this->setSecretAttribute('passphrase');
    }
@endif

    // Need a new route, you can add it by using setup*****Routes as below
/*
    protected function setup*******Routes($segment, $routeName, $controller)
    {
        $name = '********'
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'] . '/{' . $routeName . '}/'.$name, [
            'as' => $routeName . '.'.$name,
            'uses' => $controller . '@'.$name,
            'operation' => $name,
        ]);
    }

    // Now this is the method which will be called for the route above ($name)
    public function ********()
    {
        // You need this to make sure this route is accessible by this user
        $this->checkAccessOrFail();
        // return some view?
    }
*/


@if(in_array('dashboard',$operations))
    /**
     * This is the dashboard view for the more complex objects to show more detail
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function setupDashboardOperation()
    {
        // Setup some stuff

        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
        // $this->crud->setHeading("Landing: ".$this->crud->getCurrentEntry()->descriptor);

        // set the dashboard view
        $this->crud->setDashboardView('**.**.dashboard');

    }
@endif

@if(in_array('create',$operations))
    /**
     * This method is needed only if you want to do stuff after a store
     */
/*
    public function store()
    {
        // Call the upstream store command (this requires [ store as baselineStore; } when declaring the StoreOperation Trait
        $result = $this->baselineStore();
        // Check result
        if($this->isSuccessful($result)) {
            // Do something if successful
        } else {
            Log::error("Failed to store");
        }
        return $result;
    }
*/
@endif

@if(in_array('update',$operations))
    /**
     * This method is needed only if you want to do stuff after an update
     */
/*
    public function update()
    {
        // Call the upstream update command (this requires [ store as baselineUpdate; } when declaring the UpdateOperation Trait
        $result = $this->baselineUpdate();
        // Check result
        if($this->isSuccessful($result)) {
            // Do something if successful
        } else {
            Log::error("Failed to update");
        }
        return $result;
    }
*/
@endif

}
