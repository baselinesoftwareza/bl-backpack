<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2016-08-23
 * Time: 11:18 PM
 */

namespace App\Http\Requests\Admin;



use Baseline\Backpack\Helpers\RouteHelper;

class TemplateCrudRequest extends \Illuminate\Foundation\Http\FormRequest
{
    public function authorize()
    {
        return backpack_auth()->check();
    }

    public function rules(){
        $rules = [];
        $this->addCommonRules($rules);
        if(RouteHelper::is_create()) {
            $this->addCreateRules($rules);
        }
        if(RouteHelper::is_update()) {
            $this->addUpdateRules($rules);
        }
        return $rules;
    }

    public function addCommonRules(&$rules) {
        //$rules['name'] = 'required|max:255';
        //$rules['enabled'] = 'required|boolean';
    }

    public function addCreateRules(&$rules) {
        //$rules['basename'] = 'required|valid_basename|unique:form,basename,'.request('id','NULL').',id,partner_id,'.PartnerHelper::currentPartnerId();
    }

    public function addUpdateRules(&$rules) {

    }

    public function messages()
    {
        return [

        ];
    }
}
