<?php

namespace App\Models;

use Baseline\Backpack\Model\Traits\ProtectAttribute;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Baseline\Backpack\Model\Traits\IsTenant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TemplateModel extends Model
{
    use CrudTrait,
        Notifiable,
        ProtectAttribute,
        IsTenant;

    protected $table = 'tablename'; // TODO
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = [ "id" ];
    protected $fillable = [ "field_one", "field_to" ]; // TODO

    protected $noupdate = [ 'tenant_id' ];

    protected static function boot()
    {
        parent::boot();
    }

}
