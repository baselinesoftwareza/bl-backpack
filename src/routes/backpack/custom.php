<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        'tenant', // adding this for multi tenancy
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    \Baseline\Backpack\Helpers\ControllerHelper::loadControllerRoutes(config('backpack.base.route_prefix','admin'));
});

Route::group([
    'prefix'     => config('tenant.global_baseroute', 'global'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
        'global_permission',
    ],
    'namespace'  => 'App\Http\Controllers\Global',
], function () { // custom admin routes
    \Baseline\Backpack\Helpers\ControllerHelper::loadControllerRoutes(config('tenant.global_baseroute','global'));
});
