<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\TenantRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\FilterHelper;
use Baseline\Backpack\Operations\CreateOperation;
use Baseline\Backpack\Operations\DashboardOperation;
use Baseline\Backpack\Operations\DeleteOperation;
use Baseline\Backpack\Operations\ListOperation;
use Baseline\Backpack\Operations\SecretOperation;
use Baseline\Backpack\Operations\ToggleOperation;
use Baseline\Backpack\Operations\UpdateOperation;
use Baseline\Backpack\Operations\PopupOperation;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class TenantController extends CrudController
{
    use BaselineCoreController;

    // for dashboard (you WILL need a dashboard method)
    use DashboardOperation;

    // for edit/update (add { update as baselineUpdate; } if required to create update)
    use UpdateOperation;

    public static $crud_config = [
        'model' => 'App\Models\Tenant',
        'entity_name_strings' => ['company', 'companies'],
        'basename' => 'tenant',
        'icon' => 'la-cog',
    ];

    public function setup()
    {
        // Sets up the controller
        $this->setupController();
        // Any other setup thats not operation specific
    }


    /**
     * This method allows you to setup the validation, fields and other stuff for edit/update
     */
    protected function setupUpdateOperation()
    {
        // Setup validation
        $this->crud->setValidation(TenantRequest::class);
        // Setup fields
        $this->crud->field('code')->label('Company Code')
            ->type('immutable');
        $this->setupCommonFields(); // this is for fields common to both update and create
        // Update specific fields
        //$this->crud->field('field_name')->label('Nice Label')->type('text')->hint('Instruction Text');
        // Setup dependency fields (optional)
        //$this->setDependencyFields($this->getDependencyFields());
        // Set the save action (optional)
        // This redirects to a dashboard
        $this->crud->replaceSaveActions([
            'name' => 'Save',
            'redirect' => function ($crud, $request, $itemId) {
                // return to the list
                // return $this->getReturnAddress($this->crud->getCurrentEntryId()) ?? $this->crud->route;
                // return to a dashboard
                return $this->getReturnAddress($this->crud->getCurrentEntryId()) ?? $this->crud->route . "/" . $request->input('id') . "/dashboard";
            }
        ]);
        // Setup other stuff for the create page
        //$this->crud->stylesheets[] = '*****/*****.css';
        //$this->crud->scriptlets[] = 'console.log("Hi there")';
        //$this->crud->scripts[] = '**/**.js';
    }


    /**
     * This method allows you to setup common fields for update and create
     */
    private function setupCommonFields()
    {
        $this->crud->field('name')->label('Company Name')
            ->type('text');
        $this->crud->field('telephone')->label('Telephone');
        $this->crud->field('email')->label('Email')
            ->type('email');
        $entry = $this->crud->getCurrentEntry();
        // Check address, otherwise we get a nonsense location object
        $this->crud->field('location')->type("google_map")
            ->map_options([
                'height' => 600,
                'default_lng' => 18.42639,
                'default_lat' => -33.90743,
                'locate' => true,
            ])->value($entry?->address ? $entry->location : '');
        //$this->crud->field('name')->label('Client Name')->hint('The name of the client or business name');
        //$this->crud->field('enabled')->label('Enabled')->hint('Is this enabled?')->default(true);
    }

    // If you have dependency fields, you can define them here and link them into create and update
    /*
        private function getDependencyFields() {
            return [
                // this will get run as before any of the other changes (used to hide everything before something is shown)
                '_unset' => [
                    'webhook_url' => ['disabled' => true, 'parent_visible' => false ],
                    'msg_app_password' => ['disabled' => true, 'parent_visible' => false ],
                ],
                // field name or selector [name=applies_to]
                'applies_to' => [
                    // switch/case values
                    'site' => [
                        // Change this field or exec
                        'site_id' => ['disabled' => false, 'parent_visible' => true ],
                    ],
                    '*' => [
                        // Change this field or exec
                        'site_id' => ['disabled' => true, 'parent_visible' => false ],
                    ],
                ],
                // field name
                'service_id' => [
                    // switch/case values
                    '*' => [
                        // Change this field or exec
                        'documentation' => [ 'function' => 'updateInfoFromTemplate($(this).val());' ],
                    ],
                ],
            ];
        }
    */


    // Need a new route, you can add it by using setup*****Routes as below
    /*
        protected function setup*******Routes($segment, $routeName, $controller)
        {
            $name = '********'
            $config = ControllerHelper::getControllerConfigByBasename($routeName);
            Route::get($config['route'] . '/{' . $routeName . '}/'.$name, [
                'as' => $routeName . '.'.$name,
                'uses' => $controller . '@'.$name,
                'operation' => $name,
            ]);
        }

        // Now this is the method which will be called for the route above ($name)
        public function ********()
        {
            // You need this to make sure this route is accessible by this user
            $this->checkAccessOrFail();
            // return some view?
        }
    */


    public function setupDashboardOperation() {
        // Setup the dashboard
        $this->crud->setDashboardView('company.dashboard');
    }

    /**
     * This method is needed only if you want to do stuff after an update
     */
    /*
        public function update()
        {
            // Call the upstream update command (this requires [ store as baselineUpdate; } when declaring the UpdateOperation Trait
            $result = $this->baselineUpdate();
            // Check result
            if($this->isSuccessful($result)) {
                // Do something if successful
            } else {
                Log::error("Failed to update");
            }
            return $result;
        }
    */

}
