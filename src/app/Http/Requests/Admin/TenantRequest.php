<?php

namespace App\Http\Requests\Admin;

use Baseline\Backpack\Helpers\RouteHelper;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class TenantRequest extends \Illuminate\Foundation\Http\FormRequest
{
    public function authorize()
    {
        return backpack_auth()->check();
    }

    public function rules(){
        $rules = [];
        $this->addCommonRules($rules);
        if(RouteHelper::is_create()) {
            $this->addCreateRules($rules);
        }
        if(RouteHelper::is_update()) {
            $this->addUpdateRules($rules);
        }
        return $rules;
    }

    public function addCommonRules(&$rules) {
        $rules['name'] = 'required|max:255';
        $rules['location'] = 'required|json';
        $rules['telephone'] = 'required|phone:AUTO,'.implode(',',config('bl-backpack.validation.numbers_supported',[])).'|max:255';
        $rules['email'] = 'required|email:rfc,dns';
    }

    public function addCreateRules(&$rules) {
        //$rules['basename'] = 'required|valid_basename|unique:form,basename,'.request('id','NULL').',id,partner_id,'.PartnerHelper::currentPartnerId();
    }

    public function addUpdateRules(&$rules) {

    }

    public function messages()
    {
        return [
                'telephone.phone' => 'Please enter a valid phone number',
                ];
    }
}
