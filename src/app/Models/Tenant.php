<?php

namespace App\Models;

use App\Models\Traits\LogsActivity;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Baseline\Backpack\Helpers\TenantHelper;
use Baseline\Backpack\Model\Traits\LocationTrait;
use Baseline\Backpack\Model\Traits\ProtectAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tenant extends Model
{
    use CrudTrait, Notifiable, ProtectAttribute,
        LocationTrait;

    protected $table = 'tenant';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $hidden = ["id"];
    protected $fillable = ["name", "basename", "enabled", "address", "telephone", "email", "longitude", "latitude", "location"];

    protected $noupdate = [ "basename" ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($tenant) {
            if (!$tenant->basename) {
                $tenant->basename = TenantHelper::generateUniqueBasename($tenant->name, 'basename', Tenant::query());
            }
        });

        static::deleting(function ($tenant) {
            foreach ($tenant->users() as $user) {
                $user->delete();
            }
        });
    }

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
}
