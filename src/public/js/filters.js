/**
 * This method ensures that select2 filters are noted in the header
 */
$(function() {
    $('select[name^=filter_]').on('change select2:unselecting', function () {
        let select = $(this);
        let select_name = $(this).attr('name');
        let selected = $.trim(select.find(':selected').text());
        if(selected == '-') {
            selected = null;
        }
        let dropdown = select.closest('li').find('a.dropdown-toggle');
        if(!dropdown.attr('save-text')) {
            dropdown.attr('save-text',dropdown.text().trim());
            // console.log(`${select_name}: ${dropdown.attr('save-text')} was saved to save-text`);
        }
        if (selected) {
            // console.log(`${select_name}: ${selected} was selected`);
            dropdown.text(dropdown.attr('save-text') + ' - ' + selected);
            // console.log(`${select_name}: Dropdown header was set to ${dropdown.text()}`);
        } else {
            // console.log(`${select_name}: Blank was selected`);
            dropdown.text(dropdown.attr('save-text'));
            // console.log(`${select_name}: Dropdown header was reset to ${dropdown.text()}`);
        }
    }).trigger('change');
});
