function fm_apply(fields) {
    let fm_unset = fields._unset ? fields._unset : false;
    let fm_init = fields._init ? fields._init : false;
    let postSetup = [];
    postSetup.push(function() {
        if (fm_init) fm_process_actions(fm_init, true);
        if (fm_unset) fm_process_actions(fm_unset, true);
    });
    for (const fieldkey in fields) {
        if (fieldkey == '_unset') continue;
        let changes = fields[fieldkey];
        let field = $(fieldkey);
        if (fieldkey.match(/^\w+$/)) {
            field = $("[name='" + fieldkey + "']");
        }
        if (field.is('select')) {
            field.change(function () {
                if (fm_unset) fm_process_actions(fm_unset);
                //console.log(`Select ${$(this).attr('name')} changed and ${$(this).find(':selected').val()} selected`);
                fm_apply_changes_to_default(changes, $(this), fm_unset);
            });
            // run now
            postSetup.push(function () {
                //console.log(`Initialised ${field.attr('name')} as ${field.find(':selected').val()}`);
                fm_apply_changes_to_default(changes, field, fm_unset, true);
            });
        } else {
            if (field.is('input')) {
                if (field.attr('type') == 'hidden') {
                    // checkboxes aren't named anymore, so we look for the hidden field, check if the checkbox follows it (linked), then work with that
                    let checkbox_field = field.next();
                    let radios = field.parent().find('input[type=radio]');
                    if (checkbox_field && checkbox_field.attr('type') == 'checkbox') {
                        checkbox_field.on('change',function (e) {
                            if (fm_unset) fm_process_actions(fm_unset);
                            //console.log(`Checkbox ${$(this).prev().attr('name')} clicked and set checked to ${$(this).prop('checked') ?? 'false'}`);
                            fm_apply_changes_to_checkbox(changes, $(this), fm_unset);
                        });
                        // run now
                        postSetup.push(function () {
                            if (fm_unset) fm_process_actions(fm_unset);
                            //console.log(`Initialised ${field.attr('name')} as ${checkbox_field.prop('checked')}`);
                            fm_apply_changes_to_checkbox(changes, checkbox_field, fm_unset, true);
                        });
                    } else if (radios.length > 0) {
                        radios.each(function () {
                            $(this).on('change', function () {
                                if (fm_unset) fm_process_actions(fm_unset);
                                fm_apply_changes_to_default(changes, $(this), fm_unset);
                            });
                        });
                        // run now
                        postSetup.push(function () {
                            fm_apply_changes_to_default(changes, radios.first(), fm_unset, true);
                        });
                    }
                } else {
                    // We just do the work (nothing will change, so no trigger necessary)
                    postSetup.push(function () {
                        fm_apply_changes_to_checkbox(changes, field, fm_unset, true);
                    });
                }
            }
        }
    }
    for (const postSetupFunction of postSetup) {
        postSetupFunction();
    }
}

function fm_apply_changes_to_default(changes, field, unset, init_field_change = false) {
    //console.log(`Processing triggers on ${field.attr('name')}${init_field_change ? ' (init)' : ''}`);
    let mydefault = undefined;
    for (const val in changes) {
        let actions = changes[val];
        if (val == '*') {
            mydefault = actions;
        } else {
            if (field.val() == val) {
                fm_process_actions(actions, init_field_change);
                return;
            }
        }
    }
    if (mydefault) {
        fm_process_actions(mydefault, init_field_change);
    }
}

function fm_apply_changes_to_checkbox(changes, field, unset, init_field_change = false) {
    for (const state in changes) {
        let actions = changes[state];
        if (state == 'checked') {
            if (field.prop('checked')) {
                fm_process_actions(actions, init_field_change);
            }
        } else {
            if (!field.prop('checked')) {
                fm_process_actions(actions, init_field_change);
            }
        }
    }
}

function fm_process_actions(actions, init_field_change = false) {
    for (const name in actions) {
        // console.log(`  Taking actions on ${name}${init_field_change ? ' (init)' : ''}`);
        let target_field = $("[" + (name.indexOf("=") >= 0 ? name : "name='" + name + "'") + "]");
        // console.log(target_field);
        if(!target_field?.length) {
            console.log(`Failed to find field_management field ${name}`);
            continue;
        }
        let action = actions[name];

        if(!init_field_change || (init_field_change && !action.skip_init)) {
            fm_process_disabled(target_field, action, init_field_change);
            fm_process_value(target_field, action, init_field_change);
            fm_process_checked(target_field, action, init_field_change);
            fm_process_parent_visible(target_field, action, init_field_change);
            fm_process_grand_parent_visible(target_field, action, init_field_change);
            fm_process_function(target_field, action, init_field_change);
        }
    }
}

function fm_process_value(target_field, action, init_field_change = false) {
    if ('value' in action) {
        // don't erase startup values during init
        if (target_field.val() == "" || !init_field_change) {
            //console.log(`    Setting ${target_field.attr('name')} to ${action.value}${init_field_change ? ' (init)' : ''}`);
            target_field.val(action.value);
        }
    }
}

function fm_process_disabled(target_field, action, init_field_change = false) {
    if ('disabled' in action) {
        fm_enable_input(target_field, !action.disabled, init_field_change);
    }
}

function fm_process_checked(target_field, action, init_field_change = false) {
    if ('checked' in action) {
        //console.log(`    ${action.checked ? 'checking' : 'unchecking'} ${target_field.attr('name')}${init_field_change ? ' (init)' : ''}`);
        // change the attached hidden value
        target_field.val(action.checked ? 1 : 0);
        // tick the actual checkbox (next field)
        target_field.next().prop('checked', action.checked);
    }
}

function fm_process_parent_visible(target_field, action, init_field_change = false) {
    if ('parent_visible' in action) {
        // console.log(`    Making parent of ${target_field.attr('name')} ${action.parent_visible ? 'visible' : 'invisible'}${init_field_change ? ' (init)' : ''}`);
        target_field.parent().css('display', action.parent_visible ? '' : 'none');
        if(!init_field_change && action.parent_visible) {
            if (target_field.next().attr('type') == 'checkbox') {
                target_field.next().trigger("change");
                //target_field.next().trigger("click");
            } else {
                target_field.trigger("change");
                target_field.trigger("click");
            }
        }
    }
}

function fm_process_grand_parent_visible(target_field, action, init_field_change = false) {
    let key = false;
    if ('grand_parent_visible' in action) key = 'grand_parent_visible';
    if ('parent2_visible' in action) key = 'parent2_visible';
    if (key) {
        //console.log(`    Making grandparent of ${target_field.attr('name')} ${action[key] ? 'visible' : 'invisible'}${init_field_change ? ' (init)' : ''}`);
        target_field.parent().parent().css('display', action[key] ? '' : 'none');
        if(!init_field_change && action.parent_visible) {
            if (target_field.next().attr('type') == 'checkbox') {
                target_field.next().trigger("change");
                //target_field.next().trigger("click");
            } else {
                target_field.trigger("change");
                target_field.trigger("click");
            }
        }
    }
}

function fm_process_function(target_field, action, init_field_change = false) {
    if ('function' in action) {
        //console.log(`    Executing function because of change on ${target_field.attr('name')}${init_field_change ? ' (init)' : ''}`);
        eval(action.function);
    }
}

function fm_enable_input(target_field, enabled, init_field_change = false) {
    //console.log(`    ${enabled ? 'Enabling' : 'Disabling'} ${target_field.attr('name')}${init_field_change ? ' (init)' : ''}`);
    // special case of checkbox combo
    if (target_field.attr('type') == 'hidden' && target_field.next().attr('type') == 'checkbox') {
        // do the checkbox too
        target_field.next().prop('disabled', !enabled);
    }
    target_field.prop('disabled', !enabled);
}
