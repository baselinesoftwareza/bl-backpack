

function addPopulateSelect2FromSelect2(sourceSelect, destinationSelect, url_fn, options = {}) {
    sourceSelect.on('select2:clear',function() {
        destinationSelect.empty();
    });
    sourceSelect.on('select2:select',function() {
        let value = $(this).find(':selected').val();
        destinationSelect.empty();
        $.ajax({
            url: url_fn(value),
            dataType: 'json',
            success: function(data) {
                if(options.prepend_null) {
                    destinationSelect.append(new Option(options.null_text,null));
                }
                $.each(data.data, function(value,text) {
                    destinationSelect.append(new Option(text,value));
                })
                if(options.append_null) {
                    destinationSelect.append(new Option(options.null_text,null));
                }
                if(options.cleared) {
                    destinationSelect.val(null);
                }
                destinationSelect.trigger('change');
                destinationSelect.trigger('select2:select');
            }
        })
    });
}
