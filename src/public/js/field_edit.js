function setOptions(select, options, field_id, nullable) {
    select.empty();
    // if there is nothing selected, we need to add a null option anyway or a selection of the first item doesn't trigger the change
    if (nullable || !field_id) {
        select.append(new Option("-", null));
    }
    $.each(options, function (index, option) {
        select.append(new Option(option.text, option.value, false, option.value == field_id));
    });
    select.trigger('change');
    // focus the caret in the textbox
    setTimeout(() => {
        select.select2('open');
        select.data('select2').dropdown.$search.get(0).focus();
    }, 50);
}


function completeTextEdit(text_field, display_field, edit_button) {
    text_field.off('keydown');
    text_field.off('blur');
    text_field.blur();
    text_field.hide();
    text_field.val("");
    display_field.show();
    edit_button.show();
}

function textFieldEditSetValue(value, display_field, field_edit) {
    let display_value = value;
    display_field.attr('data-original-title', value);
    if (value.length > field_edit.data('fieldLimit')) {
        display_value = value.substring(0, field_edit.data("fieldLimit")) + "...";
        display_field.attr('data-toggle', 'tooltip');
        display_field.tooltip();
    }
    display_field.text(display_value);
    field_edit.data('fieldValue', value);
    field_edit.attr('data-field-value', value);
}

function saveTextEdit(text_field, field_edit, display_field) {
    let prev_value = field_edit.data('fieldValue');
    let value = text_field.val();
    textFieldEditSetValue(value, display_field, field_edit);
    field_update(field_edit, value, function (field_edit) {
        textFieldEditSetValue(prev_value, display_field, field_edit);
    });
}

function saveTextEdit2(text_field, field_edit, display_field,edit_button) {
    let prev_value = field_edit.data('fieldValue');
    let value = text_field.val();
    field_update2(field_edit, value, function (field_edit) {
        text_field.val('');
        text_field.hide();
        display_field.show();
        edit_button.show();
    });
}

function completeTextEdit2(text_field, display_field, edit_button) {
    text_field.off('keydown');
    text_field.off('blur');
    text_field.blur();
    text_field.prop('disabled', true);
}

function processTextFieldEdits2(scope = null) {
    (scope ?? $(document)).find('.field_edit2_text.field_edit2').find('button').on('click', function (e) {
        let field_edit = $(this).parent();
        let edit_button = $(this);
        let text_field = field_edit.find('input');
        let field_value = field_edit.data('fieldValue');
        let field = field_edit.data('field');
        let entry_id = field_edit.data('entryId');
        let display_field = field_edit.find('span').first();
        display_field.hide();
        edit_button.hide();
        text_field.prop('disabled', false);
        text_field.val($.trim(field_value));
        text_field.show();
        text_field.focus();
        text_field.select();
        text_field.on('keydown', function (e) {
            if (e.keyCode === 13) {
                // enter was pressed
                saveTextEdit2(text_field, field_edit, display_field, edit_button);
                completeTextEdit2(text_field, display_field, edit_button);
            } else if (e.keyCode === 27) {
                // escape was pressed
                completeTextEdit(text_field, display_field, edit_button);
                call_trigger(field_edit.data('triggerCancel'));
            }
        }).on('blur', function (e) {
            // focus moved away
            saveTextEdit2(text_field, field_edit, display_field,edit_button);
            completeTextEdit2(text_field, display_field, edit_button);
        })
    });
}

function processTextFieldEdits(scope = null) {
    (scope ?? $(document)).find('.field_edit_text.field_edit').find('button').on('click', function (e) {
        let field_edit = $(this).parent();
        let edit_button = $(this);
        let text_field = field_edit.find('input');
        let field_value = field_edit.data('fieldValue');
        let field = field_edit.data('field');
        let entry_id = field_edit.data('entryId');
        let display_field = field_edit.find('span').first();
        display_field.hide();
        edit_button.hide();
        text_field.val($.trim(field_value));
        text_field.show();
        text_field.focus();
        text_field.select();
        text_field.on('keydown', function (e) {
            if (e.keyCode === 13) {
                // enter was pressed
                saveTextEdit(text_field, field_edit, display_field);
                completeTextEdit(text_field, display_field, edit_button);
            } else if (e.keyCode === 27) {
                // escape was pressed
                completeTextEdit(text_field, display_field, edit_button);
            }
        }).on('blur', function (e) {
            // focus moved away
            saveTextEdit(text_field, field_edit, display_field);
            completeTextEdit(text_field, display_field, edit_button);
        })
    });
}

function call_trigger(js_function, data = null) {
    if (js_function) {
        let fn = window[js_function];
        if (typeof fn == 'function') {
            fn(data);
        }
    }
}

function field_update2(field_edit, new_value, undo_function) {
    let field = field_edit.data('field');
    let entry_id = field_edit.data('entryId');
    let field_update_url = field_edit.data('updateUrl');
    field_edit.find('.update_success').hide();
    field_edit.find('.update_error').hide();
    field_edit.find(".update_spinner").show();

    $.ajax({
        url: field_update_url ?? `${window.location.pathname}/${entry_id}/field_edit2`,
        dataType: 'json',
        method: 'POST',
        data: {
            'field': field,
            [field]: new_value
        },
        success: function (data) {
            let row = field_edit.closest('tr');
            // foreach td, replace with corresponding data.data[0] element
            row.find('td').each(function (index, td) {
                let td_data = data.data[0][index];
                if (td_data) {
                    $(td).html(td_data);
                    // if the index is in the data.changed_columns, then add a class to the td
                    if (data.changed_columns.includes(index)) {
                        $(td).attr('class','field_edit_updated');
                        setTimeout(function () {
                            $(td).attr('class','field_edit_updated_fade');
                        }, 500);
                        // $(td).removeClass('field_edit_updated');
                    }
                }
            });
            processFieldEdits(row);
            call_trigger(field_edit.data('triggerUpdated'), data);
        },
        error: function (xhr) {
            field_edit.find('.update_error').show();
            // lets reset our original values
            undo_function(field_edit);
            // lets alert
            let response = xhr.responseText;
            if (response) {
                try {
                    let data = JSON.parse(response);
                    new Noty({
                        type: "error",
                        text: 'Failed to update ' + field_edit.data('fieldLabel') + " : " + data.message,
                    }).show();
                    call_trigger(field_edit.data('triggerError'), data);
                    return;
                } catch {
                    // not json
                }
            }
            new Noty({
                type: "error",
                text: 'Failed to update ' + field_edit.data('fieldLabel'),
            }).show();
            call_trigger(field_edit.data('triggerError'), data);
        },
        complete: function () {
            field_edit.find(".update_spinner").hide();
            setTimeout(function () {
                // field_edit.find('.update_error').hide();
                field_edit.find('.update_success').hide();
            }, 5000);
        }
    })
}

function field_update(field_edit, new_value, undo_function) {
    let field = field_edit.data('field');
    let entry_id = field_edit.data('entryId');
    let field_update_url = field_edit.data('updateUrl');
    field_edit.find('.update_success').hide();
    field_edit.find('.update_error').hide();
    field_edit.find(".update_spinner").show();

    $.ajax({
        url: field_update_url ?? `${window.location.pathname}/${entry_id}/field_edit`,
        dataType: 'json',
        method: 'POST',
        data: {
            'field': field,
            [field]: new_value
        },
        success: function (data) {
            field_edit.find('.update_success').show();
            let scope_closest = field_edit.data('scope_closest') ?? 'tr';
            let row = field_edit.closest(scope_closest);
            // find all the editable fields and update them
            row.find('.field_edit .field_dependant').each(function () {
                let update_field_edit = $(this);
                let update_field = update_field_edit.data('field');
                let update_field_id = update_field_edit.data('updateFieldId');
                let update_field_escaped = update_field_edit.data('fieldEscaped');
                if (update_field_id) {
                    let id = bl_simple_template(update_field_id, data);
                    // I want these to be both the same (in sync)
                    update_field_edit.attr('data-field-id', id);
                    update_field_edit.data('fieldId', id);
                }
                let update_field_value = update_field_edit.data('updateFieldValue');
                let fn = window[update_field_value];
                if (typeof fn == 'function') {
                    update_field_value = fn(data[update_field], data);
                }
                // this is the actual update
                if (update_field_value) {
                    let value = bl_simple_template(update_field_value, data);
                    update_field_edit.attr('data-field-value', value);
                    update_field_edit.data('fieldValue', value);
                    if (update_field_edit.hasClass('field_edit_text')) {
                        let limit = update_field_edit.data('fieldLimit');
                        let text = value ?? "";
                        if (limit && text.length > limit) {
                            text = text.substring(0, limit) + '...';
                        }
                        if (update_field_escaped) {
                            update_field_edit.find('span:first').text(text);
                        } else {
                            update_field_edit.find('span:first').html(text);
                        }
                    } else {
                        if (update_field_escaped) {
                            update_field_edit.find('.data-field-display').text(value);
                        } else {
                            update_field_edit.find('.data-field-display').html(value);
                        }
                    }
                    if (!value) {
                        // set the null selector
                        update_field_edit.find('.data-field-null').show();
                    }
                }
                let field_url = update_field_edit.data('url');
                if (field_url) {
                    update_field_edit.find('a.data-field-display').attr('href', bl_simple_template(field_url, data));
                }
                let update_tooltip = update_field_edit.data('tooltip');
                if (update_tooltip) {
                    let tooltip_element = null;
                    if (update_field_edit.hasClass('field_edit_text')) {
                        tooltip_element = update_field_edit.find('span');
                    } else {
                        tooltip_element = update_field_edit.find('.data-field-display');
                    }
                    if (tooltip_element) {
                        tooltip_element.attr('data-original-title', bl_simple_template(update_tooltip, data));
                    }
                }
            });
            row.trigger('field_edit.updated', [data]);
        },
        error: function (xhr) {
            field_edit.find('.update_error').show();
            // lets reset our original values
            undo_function(field_edit);
            // lets alert
            let response = xhr.responseText;
            if (response) {
                try {
                    let data = JSON.parse(response);
                    new Noty({
                        type: "error",
                        text: 'Failed to update ' + field_edit.data('fieldLabel') + " : " + data.message,
                    }).show();
                    return;
                } catch {
                    // not json
                }
            }
            new Noty({
                type: "error",
                text: 'Failed to update ' + field_edit.data('fieldLabel'),
            }).show();
        },
        complete: function () {
            field_edit.find(".update_spinner").hide();
            setTimeout(function () {
                // field_edit.find('.update_error').hide();
                field_edit.find('.update_success').hide();
            }, 5000);
        }
    })
}

function processSelect2FieldEdits(scope= null) {
    (scope ?? $(document)).find('.field_edit_select2.field_edit').on('show.bs.dropdown', function () {
        let field_edit = $(this);
        let nullable = field_edit.data('nullable');
        let options = field_edit.data('options');
        let field_id = field_edit.data('fieldId');
        let field = field_edit.data('field');
        let entry_id = field_edit.data('entryId');
        let dropdown_menu = field_edit.find('.dropdown-menu');
        let select = dropdown_menu.find('select');
        if (!select.hasClass('select2-hidden-accessible')) {
            // only create the select2 once
            select.select2({
                allowClear: true,
                closeOnSelect: false,
                theme: "bootstrap",
                dropdownParent: select.parent('.form-group'),
                placeholder: "",
            }).on('select2:unselecting', function (e) {
                //
                select.val(null)
                field_edit.removeClass("active");
                dropdown_menu.removeClass("show");

                // e.stopPropagation();
                // return false;
            }).on('select2:select', function (e) {
                field_edit.removeClass("active");
                dropdown_menu.removeClass("show");
                // }).on('change', function(e) {
                let new_id = $(this).val();
                if (new_id === "null") {
                    new_id = null;
                }
                let display_value = $(this).find('option:selected').text();

                // console.log(`Change ${field_id} to ${value}`);
                if (display_value == '-') {
                    field_edit.find('.data-field-display').text("");
                    field_edit.find('.data-field-null').show();
                } else {
                    field_edit.find('.data-field-display').text(display_value);
                    field_edit.data('fieldId', new_id);
                    field_edit.attr('data-field-id', new_id);
                }

                if (new_id) {
                    field_edit.find('.data-field-null').hide();
                }

                let prev_text = field_edit.find('.data-field-display').text();
                let prev_id = field_id;

                field_update(field_edit, new_id, function (field_edit) {
                    field_edit.find('.data-field-display').text(prev_text);
                    field_edit.data('fieldId', prev_id);
                    field_edit.attr('data-field-id', prev_id);
                });
            });
        }


        // now lets populate options
        if (typeof options == 'string') {
            let json = false;
            try {
                options = JSON.parse(options);
                json = true;
            } catch (error) {
            }

            if (!json) {
                let fn = window[options];
                if (typeof fn == 'function') {
                    // call the method with the success function
                    fn(field_edit, function (options) {
                        setOptions(select, options, field_id, nullable);
                    });
                }
            }
        }

        // if we got an array out then process it
        if (Array.isArray(options) || typeof options == 'object') {
            setOptions(select, options, field_id, nullable);
        }
    });
}

function processFieldEdits(scope = null) {
    scope ??= $(document);
    processSelect2FieldEdits(scope);
    processTextFieldEdits(scope);
    processTextFieldEdits2(scope);
}

$(function () {
    $('#crudTable').on('draw.dt', function () {
        processFieldEdits();
    })
});
