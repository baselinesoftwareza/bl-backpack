function initBasenameGenerators() {
    $("[data-toggle=basename]").each(function () {
        let basename = $(this);
        let generateFromFieldName = basename.data('generate-from');
        let maxLength = Math.max(basename.data('max-length'), 32);
        let noSpacer = basename.data('no-dash') ?? 'false';
        let spacerChar = basename.data('spacer-char') ?? '-';
        if (generateFromFieldName) {
            let generateFromField = $('input[name=' + generateFromFieldName + ']');
            generateFromField?.on('keyup', function (e) {
                basename.val(transformToBasename(generateFromField.val(), maxLength, noSpacer, spacerChar, false));
            });
        }
        basename.on('keyup', function (e) {
            $(this).val(transformToBasename($(this).val(), maxLength, noSpacer, spacerChar, true));
        });
        basename.on('change', function (e) {
            $(this).val(transformToBasename($(this).val(), maxLength, noSpacer, spacerChar, false));
        });
    });
}

function transformToBasename(text, maxLength, noSpacer, spacerChar, direct = true) {
    let thistext = text.replace(/(_| |-)+/g, noSpacer ? '' : spacerChar)
        .replace(new RegExp(`[^\\w${spacerChar}]`, 'g'), '').toLowerCase();
    if (!direct) {
        // trim bad characters from end if generating
        thistext = thistext.replace(new RegExp(`[0-9${spacerChar}]+$`), '');
    }
    thistext = thistext.replace(new RegExp(`^[0-9${spacerChar}]+`), '')
        .substring(0, maxLength);
    if (!direct) {
        thistext = thistext.replace(new RegExp(`[0-9${spacerChar}]+$`), '');
    }
    return thistext;
}

$(function () {
    initBasenameGenerators();
});

