function init_map(initialLatitude, initialLongitude, mapDiv, searchBox, latitudeBox, longitudeBox, addressBox, zoom = 14) {

    var latlng;
    if(longitudeBox.val() > 0 && (latitudeBox.val() > 0 || latitudeBox.val() < 0)) {
        latlng = new google.maps.LatLng(latitudeBox.val(), longitudeBox.val());
    } else {
        latlng = new google.maps.LatLng(initialLatitude, initialLongitude);
    }
    // Map setup options
    var mapOptions = {
        zoom: zoom,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    // The geocoder properties
    var mapper = {
        geocoder: new google.maps.Geocoder(),
        pos: latlng,
        map: new google.maps.Map(mapDiv[0], mapOptions),
        marker: null,
        infowindow: new google.maps.InfoWindow({size: new google.maps.Size(150, 50)}),
        searchBox: new google.maps.places.SearchBox(searchBox[0]),
        latitudeBox: latitudeBox,
        longitudeBox: longitudeBox,
        addressBox: addressBox
    };

    // below are generic event handlers

    // Closes the window if we click elsewhere
    google.maps.event.addListener(mapper.map, 'click', function () {
        mapper.infowindow.close();
    });

    // Positions the search box
    mapper.map.controls[google.maps.ControlPosition.TOP_LEFT].push(searchBox[0]);

    // when the bounds change, make sure the search box is aware
    mapper.map.addListener('bounds_changed', function () {
        mapper.searchBox.setBounds(mapper.map.getBounds());
    });


    // Setup Initial Marker
    setupMarker(mapper, false);

    // Setup Places Search
    initPlaces(mapper);
}

function clone(obj) {
    if (obj == null || typeof(obj) != 'object') return obj;
    var temp = new obj.constructor();
    for (var key in obj) temp[key] = clone(obj[key]);
    return temp;
}

function geocodePosition(mapper, mod_address) {
    mapper.geocoder.geocode({
        latLng: mapper.marker.getPosition()
    }, function (responses) {
        if (responses && responses.length > 0) {
            mapper.marker.formatted_address = responses[0].formatted_address;
        } else {
            mapper.marker.formatted_address = 'Cannot determine address at this location.';
            console.log("Coult not determine the address at location : " + mapper.pos );
        }
        // Set the info box
        mapper.infowindow.setContent(mapper.marker.formatted_address + "<br>coordinates: " + mapper.marker.getPosition().toUrlValue(6));
        mapper.infowindow.open(mapper.map, mapper.marker);
        // Set the lat, long and address from the marker
        mapper.latitudeBox.val(mapper.marker.getPosition().lat());
        mapper.longitudeBox.val(mapper.marker.getPosition().lng());
        if(mod_address) {
            mapper.addressBox.val(mapper.marker.formatted_address.replace(/(,\s*)+/g, "\n"));
        }
    });
}

/*
function codeAddress(address) {
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            if (marker) {
                marker.setMap(null);
                if (infowindow) infowindow.close();
            }
            setupMarker(map.getCenter());
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}
*/

function setupMarker(mapper, mod_address) {
    // place the marker
    if(mapper.marker == null) {
        mapper.marker = new google.maps.Marker({
            map: mapper.map,
            draggable: true,
            position: mapper.pos
        });

        // Setup the drag event (re-geocode)
        google.maps.event.addListener(mapper.marker, 'dragend', function () {
            geocodePosition(mapper,true);
        });

        // Setup the click event to show the address if the marker is clicked
        google.maps.event.addListener(mapper.marker, 'click', function () {
            // if we have an address, show it
            if (mapper.marker.formatted_address) {
                mapper.infowindow.setContent(mapper.marker.formatted_address + "<br>coordinates: " + mapper.marker.getPosition().toUrlValue(6));
            } else {
                // if we don't have an address, geocode the marker
                geocodePosition(mapper,true);
            }
            mapper.infowindow.open(mapper.map, mapper.marker);
        });

    } else {
        mapper.marker.setPosition(mapper.pos);
    }
}

function initPlaces(mapper) {
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    mapper.searchBox.addListener('places_changed', function () {
        var places = mapper.searchBox.getPlaces();

        if (places.length == 0) {
            // nothing selected
            return;
        }

        var bounds = new google.maps.LatLngBounds();
        // Select first valid place
        places.some(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                // go to next iteration
                return false;
            }
            mapper.pos = place.geometry.location;
            mapper.marker.setPosition(place.geometry.location);

            geocodePosition(mapper,true);
            google.maps.event.trigger(mapper.marker, 'click');

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
            // break the loop
            return true;
        });
        mapper.map.setZoom(16);
    });
}
