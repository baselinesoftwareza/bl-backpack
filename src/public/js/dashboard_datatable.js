var dashboard_datatables = [];

$('.dashboard-datatable').each(function () {
    let datatable = $(this);
    let ddt_id = datatable.attr('id');
    let columns = [];
    let order = [];

    datatable.find('thead > tr > th').each(function (index) {
        let col = $(this);
        columns.push({
            data: (col.data('name') ?? null),
            defaultContent: col.data('default') ?? '',
            orderable: col.data('orderable') ?? true,
            visible: col.data('visible') ?? true,
            render: col.data('render') ? function (data, type, row, meta) {
                // console.log(col.data('render'));
                return window[col.data('render')](data, type, row, meta, col.data());
            } : null,
        });
        if (col.data('order')) {
            order.push([index, col.data('order-direction') ?? 'desc']);
        }
    });

    let dt = dashboard_datatables[ddt_id] = {
        dt: datatable.DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            order: order,
            dom:
                (datatable.data('no-topbar') ? '' : "<'row'<'col-sm-4'f><'col-sm-4'i><'col-sm-4'p>>") +
                "<'row'<'col-sm-12'tr>>" +
                (datatable.data('no-bottombar') ? '' : "<'row'<'col-sm-6'l><'col-sm-6'p>>"),
            //searchDelay: 1000,
            ajax: {
                url: datatable.data('url'),
                dataSrc: function (json) {
                    let data = json.data;
                    if (datatable.data('fn-data')) {
                        let fn = window[datatable.data('fn-data')];
                        if (fn) {
                            data = fn(json) ?? data;
                        }
                    }
                    return data;
                }
            },
            columns: columns,
            createdRow: function (row, data, index) {
                if (data._meta?.highlight) {
                    $(row).addClass('dt-highlight');
                }
            }
        }),
        timeout: null
    };

    $('.dataTables_filter').addClass('float-left');
    $('.dataTables_info').addClass('text-center');
    datatable.on('draw.dt', function () {
        // add field edit support
        processFieldEdits();

        let refresh = datatable.data('refresh');
        if(refresh) {
            if (dt.dt.timeout != null) {
                clearTimeout(dt.dt.timeout);
                dt.dt.timeout = null;
            }
            dt.dt.timeout = setTimeout(function () {
                dt.dt.timeout = null;
                dt.dt.ajax.reload(null, false);
            }, refresh * 1000)
        }
    });
});

function ddt_render_field_edit_text(data, type, row, meta, column) {
    let settings = column.settings ?? [];
    let escaped = !!column?.escaped;
    let update_url = column.update_url ? bl_simple_template(column.update_url,row) : "";
    let value = ddt_get_value(data,type,row,meta,column);
    let text = ddt_render_simple_value(column, value) ?? "";
    let limit = column.limit ?? 32;
    let result = `<div class='field_edit field_edit_text' style='display: flex; align-items: center; padding: 0.275rem 0.0rem;'
                 data-nullable='${(!!column.nullable)}'
                 data-field='${column.name ?? ""}'
                 data-field-label='${column.label ?? ""}'
                 data-field-value='${data ?? ""}'
                 data-field-limit='${limit}'
                 data-entry-id='${row.id ?? ""}'
                 data-field-escaped="${escaped}"
                 data-update-field-value='${column.value ?? "{"+column.name+"}"}'
                 data-tooltip='${settings.tooltip ?? ""}'
                 data-update-url='${update_url ?? ""}'
                 data-other='${JSON.stringify(column.other ?? [])}'
    >`;

    result += `<span`;
    if(text.length > limit) {
        result += ` data-toggle='tooltip' title='${text}'`;
        text = text.substring(0,limit) + "...";
    }
    result += `>${text}`;
    result += `</span>`;
    result += `<button class="btn btn-sm"
       data-toggle="tooltip" title="Edit">
        <span class="data-field-null" style="display: ${data ? 'none' : 'unset'}">${settings.null_select_label ?? ""}</span>
        <span><i class="la la-edit"></i></span>
    </button>
    <input type="text" name="text" style="display: none; width: 90%" />
    <div class="pl-1">
        <div class="update_spinner spinner-grow spinner-grow-sm" style="display: none"><span class="sr-only">Updating...</span></div>
        <div class="update_success" style="display: none; color: rgb(54 165 54)"><i class="la la-check"></i></div>
        <div class="update_error" style="display: none; color: #FF0000"><i class="la la-times"></i></div>
    </div>`;
    result += "</div>";
    return result;
}

function encodeDropdownOptions(column) {
    if(typeof column.dropdown_options == 'string') {
        return column.dropdown_options;
    }
    return JSON.stringify(column.dropdown_options);
}

function ddt_render_field_edit_select2(data, type, row, meta, column) {
    let settings = column.settings ?? [];
    let update_url = column.update_url ? bl_simple_template(column.update_url,row) : "";
    let value = ddt_get_value(data,type,row,meta,column);
    let result = `<div class='field_edit field_edit_select2' style='display: flex; align-items: center; padding: 0.375rem 0rem;'
                 data-nullable='${(!!column.nullable)}'
                 data-field='${column.name ?? ""}'
                 data-field-label='${column.label ?? ""}'
                 data-field-id='${data ?? ""}'
                 data-entry-id='${row.id ?? ""}'
                 data-update-url='${update_url ?? ""}'
                 data-update-field-id='${("{"+column.name+"}") ?? ""}'
                 data-update-field-value='${column.value ?? "{"+column.name+"}"}'
                 data-field-escaped="${!!column?.escaped}"
                 data-tooltip='${settings.tooltip ?? ""}'
                 data-url='${settings.url ?? ""}'
                 data-options='${column.dropdown_options ? encodeDropdownOptions(column) : "[]"}'
                 data-other='${JSON.stringify(column.other ?? [])}'
    >`;
    let open_tag = '<span';
    let close_tag = '</span>';
    if ('url' in settings) {
        open_tag = `<a href="${bl_simple_template(settings.url,row)}"`;
        close_tag = '</a>'
    }
    result += `${open_tag} class="data-field-display"`;
    if ('tooltip' in settings) {
        result += `data-toggle="tooltip" title="${bl_simple_template(settings.tooltip,row)}"`;
    }
    result += `>${ddt_render_simple_value(column, value)}${close_tag}`;

    result += `<a href="#" class="nav-link dropdown-toggle p-0"
       data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="data-field-null" style="display: ${data ? 'none' : 'unset'}">${settings.null_select_label ?? ""}</span>
    </a>
    <div class="dropdown-menu">
        <div class="form-group backpack-filter mb-0">
            <select class="form-control input-sm select2">
            </select>
        </div>
    </div>
    <div class="pl-1">
        <div class="update_spinner spinner-grow spinner-grow-sm" style="display: none"><span class="sr-only">Updating...</span></div>
        <div class="update_success" style="display: none; color: rgb(54 165 54)"><i class="la la-check"></i></div>
        <div class="update_error" style="display: none; color: #FF0000"><i class="la la-times"></i></div>
    </div>`;
    result += "</div>";
    return result;
}

function ddt_render_simple_value(column, value) {
    let result = "";
    if (column.escaped ?? true) {
        result = $('<div/>').text(value ?? "").html();
    } else {
        result = value;
    }
    return result ?? "";
}

function ddt_get_value(data, type, row, meta, column) {
    if('value' in column) {
        let fn = window[column.value];
        if(fn) {
            return fn(data,type,row,meta,column) ?? "";
        }
        return bl_simple_template(column.value, row) ?? "";
    }
    return data ?? "";
}
