<?php


namespace Baseline\Backpack\Transformers;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class FractalTransformer extends BaseTransformer
{
    private $fractal = [];
    private $includes = [];
    private $excludes = [];
    private bool $include_null = true;

    // this needs a public $fractal property on the model

    public function __construct(string|Model|array $obj, $include_null = true, $only = [], $includes = [], $excludes = [])
    {
        $this->includes = $includes;
        $this->excludes = $excludes;
        $this->include_null = $include_null;
        if($only) {
            $this->fractal = $only;
        } else {
            if($obj instanceof string) {
                // create a model
                $obj = new $obj();
            }
            if (is_object($obj)) {
                if($obj instanceof Model) {
                    $this->obj = $obj;
                    $prop = 'fractal';
                    if (property_exists($obj, $prop)) {
                        $this->fractal = $obj->$prop ?? [];
                    } else {
                        $this->fractal = $obj->getFillable();
                    }
                } else {
                    throw new \Exception("Cannot use $obj as source of fields, it must be a class, model or array");
                }
            } else {
                $this->fractal = $obj;
            }
        }
        $this->fractal = array_diff([...($this->fractal), ...($this->includes)],$this->excludes);
//        Log::debug($obj);
    }

    public function transform($obj)
    {
//        Log::debug($this->fractal);
        return $this->smap($this->fractal, $obj, $this->include_null);
    }
}
