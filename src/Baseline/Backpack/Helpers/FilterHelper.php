<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 21/04/2020
 * Time: 00:10
 */

namespace Baseline\Backpack\Helpers;


use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Illuminate\Database\Eloquent\Builder;

class FilterHelper
{
    public static function addEnabledFilter(CrudPanel $crud, $label, $field = 'enabled')
    {
        $crud->addFilter(['type' => 'simple', 'name' => $field == 'enabled' ? 'enabled' : 'enabled_' . $field, 'label' => $label],
            false,
            function () use ($crud, $field) {
                $crud->addClause('where', $crud->model->getTable() . "." . $field, 1);
            });

    }

    public static function addNotNullFilter(CrudPanel $crud, $label, $field)
    {
        $crud->addFilter(['type' => 'simple', 'name' => 'not_null_' . $field, 'label' => $label],
            false,
            function () use ($crud, $field) {
                $crud->addClause('whereNotNull', $crud->model->getTable() . "." . $field);
            });
    }

    public static function addNullFilter(CrudPanel $crud, $label, $field)
    {
        $crud->addFilter(['type' => 'simple', 'name' => 'null_' . $field, 'label' => $label],
            false,
            function () use ($crud, $field) {
                $crud->addClause('whereNull', $crud->model->getTable() . "." . $field);
            });

    }


    /**
     * @param CrudPanel $crud
     * @param $label string label of the filter
     * @param $list array list of records to show
     * @param $whereHasAttribute string attribute of the host record which has the sub records
     * @param $key_attribute string attribute in the sub records defining the key
     * @param $value_attribute string attribute in the sub records defining the shown value in the filter
     * @param $foreign_key_attribute string attribute attribute in the sub records which reference the $key_attribute in the list
     */
    public static function addWhereHasFilter(CrudPanel $crud, $label, $list, $whereHasAttribute, $key_attribute, $value_attribute, $foreign_key_attribute)
    {
        // gets the list
        $selection = [];
        foreach ($list as $element) {
            $selection[$element->{$key_attribute}] = $element->{$value_attribute};
        }
        $crud->addFilter(['type' => 'dropdown', 'name' => 'wherehas_' . $whereHasAttribute, 'label' => $label],
            $selection,
            function ($value) use ($crud, $whereHasAttribute, $foreign_key_attribute) {
                $crud->addClause('whereHas', $whereHasAttribute, function (Builder $q) use ($value, $foreign_key_attribute) {
                    $q->where($foreign_key_attribute, $value);
                });
            });

    }

    /**
     * @param CrudPanel $crud
     * @param $label string label of the filter
     * @param $select array list of key->value options
     * @param $attribute string attribute of the host record which must be matched
     */
    public static function addInArrayFilter(CrudPanel $crud, $label, $selection, $attribute)
    {
        $crud->addFilter(['type' => 'dropdown', 'name' => 'whereInArray_' . $attribute, 'label' => $label],
            $selection,
            function ($value) use ($crud, $attribute) {
                $crud->addClause('where', $crud->model->getTable() . "." . $attribute, $value);
            });
    }

    public static function addDateRange(CrudPanel $crud, $label = 'Date Range', $attribute = 'created_at', $date_range_options = [])
    {
        $crud->addFilter(['type' => 'date_range', 'name' => $attribute . "_filter", 'label' => $label, 'date_range_options' => $date_range_options],
            false,
            function ($value) use ($crud, $attribute) {
                $dates = json_decode($value);
                $crud->addClause('where', $attribute, '>=', $dates->from . " 00:00:00");
                $crud->addClause('where', $attribute, '<=', $dates->to . " 23:59:59");
            });

    }
}
