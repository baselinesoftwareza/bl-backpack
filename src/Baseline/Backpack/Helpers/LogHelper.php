<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 03/05/2020
 * Time: 11:49
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Facades\Log;

class LogHelper
{
    public static function hack($message) {
        $user = backpack_user() ? backpack_user()->email : 'Unknown (command line?)';
        $requester_ip = MiscHelper::getRequesterIp();
        Log::stack([ ...(in_array('hack',Log::getChannels() ?? []) ? ['hack'] : []),'stack'])
            ->critical($message.": [user='$user',ip='$requester_ip']");
    }
}
