<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2016/09/26
 * Time: 2:37 PM
 */

namespace Baseline\Backpack\Helpers;


class SecurityHelper {

    public static function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        if($keyspace == null || $keyspace == "") {
            $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    public static function random_str_unique_in_db($length, $query, $field_name, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', $prefix = "") {
        $key = "";
        do {
            $key = $prefix.self::random_str($length, $keyspace);
            $try_query = clone $query;
        } while ($try_query->where($field_name,$key)->first());
        return $key;
    }

    public static function random_prefixed_str_unique_in_db($length, $query, $field_name, $prefix, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $key = "";
        do {
            $key = $prefix.self::random_str($length, $keyspace);
            $try_query = clone $query;
        } while ($try_query->where($field_name,$key)->first());
        return $key;
    }

    static function sshapasswd($input){
        mt_srand((double)(microtime(true) ^ posix_getpid()));
        $salt = pack("CCCC", mt_rand(0,255), mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));

        $passwd_sha1 = sha1($input . $salt, TRUE);

        $result = '{SSHA}' . base64_encode($passwd_sha1 . $salt);

        if (!self::checkssha($result, $input))
            return null;
        else
            return $result;

    }

    static function checkssha ($input, $passwd){
        $orig = base64_decode(substr($input, 6));
        $hash = substr($orig, 0, 20);
        $salt = substr($orig, 20, 4);

        if (sha1($passwd . $salt, TRUE) == $hash){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    static function randomMacAddress() {
        $mac_parts = [];
        for($i = 0; $i < 6; $i++) {
            $mac_parts[] = self::random_str(2,'0123456789ABCDEF');
        }
        return implode(":",$mac_parts);
    }
}
