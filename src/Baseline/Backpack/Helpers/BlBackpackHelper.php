<?php

namespace Baseline\Backpack\Helpers;

class BlBackpackHelper
{
    /**
     * Transforms a given text into a basename format.
     *
     * @param string  $text        The text to transform.
     * @param int     $maxLength   The maximum length of the transformed text.
     * @param bool    $noSpacer    Determines if spaces, underscores and hyphens should be removed or replace by spacer.
     * @param string  $spacerChar  The character used to replace spaces, underscores and hyphens.
     * @param bool    $generating      Determines if the transformation is complete or currently generating.
     *
     * @return string  The transformed text.
     */
    
    static function transformToBasename($text, $maxLength, $noSpacer, $spacerChar, $generating = true) {
        $thistext = preg_replace('/(_| |-)+/', $noSpacer ? '' : $spacerChar, $text);
        $thistext = preg_replace('/[^\\w' . preg_quote($spacerChar) . ']/', '', strtolower($thistext));

        if (!$generating) {
            // trim bad characters from end if generating
            $thistext = preg_replace('/[0-9' . preg_quote($spacerChar) . ']+$/', '', $thistext);
        }

        $thistext = preg_replace('/^[0-9' . preg_quote($spacerChar) . ']+/', '', $thistext);
        $thistext = substr($thistext, 0, $maxLength);

        if (!$generating) {
            $thistext = preg_replace('/[0-9' . preg_quote($spacerChar) . ']+$/', '', $thistext);
        }

        return $thistext;
    }

}
