<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 24/04/2020
 * Time: 16:37
 */

namespace Baseline\Backpack\Helpers;

use Illuminate\Support\Str;

final class ClassFinder
{
    private static $composer = null;
    private static $classes  = [];

    public function __construct()
    {
        self::$composer = null;
        self::$classes  = [];

        self::$composer = require base_path('/vendor/autoload.php');

        if (false === empty(self::$composer)) {
            self::$classes  = array_keys(self::$composer->getClassMap());
        }
    }

    public function getClasses()
    {
        $allClasses = [];

        if (false === empty(self::$classes)) {
            foreach (self::$classes as $class) {
                $allClasses[] = '\\'.$class;
            }
        }

        return $allClasses;
    }

    public function getClassesByNamespace($namespace)
    {
        if(is_scalar($namespace)) {
            $namespace = [$namespace];
        }
        for($i = 0; $i < count($namespace);$i++) {
            if (0 !== strpos($namespace[$i], '\\')) {
                $namespace[$i] = '\\' . $namespace[$i];
            }
            $namespace[$i] = strtoupper($namespace[$i]);
        }

//        $termUpper = strtoupper($namespace);
        return array_filter($this->getClasses(), function($class) use ($namespace) {
            $className = strtoupper($class);
            foreach($namespace as $termUpper) {
                if (
                    0 === strpos($className, $termUpper) and
                    false === strpos($className, strtoupper('Abstract')) and
                    false === Str::endsWith($className, strtoupper('Interface'))
                ) {
                    return $class;
                }
            }
            return false;
        });
    }

    public function getClassesWithTerm($term)
    {
        $termUpper = strtoupper($term);
        return array_filter($this->getClasses(), function($class) use ($termUpper) {
            $className = strtoupper($class);
            if (
                false !== strpos($className, $termUpper) and
                false === strpos($className, strtoupper('Abstract')) and
                false === strpos($className, strtoupper('Interface'))
            ){
                return $class;
            }
            return false;
        });
    }
}
