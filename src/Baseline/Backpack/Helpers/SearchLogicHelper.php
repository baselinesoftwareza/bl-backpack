<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 21/10/2018
 * Time: 12:14
 */

namespace Baseline\Backpack\Helpers;


use Illuminate\Database\Query\Builder;

class SearchLogicHelper
{
    /**
     * @param $query Builder query to modify
     * @param $searchTerm string search term to search for
     * @param $relation string table to search in
     * @param $fields array fields to search within said table
     */
    public static function searchSub($query, $searchTerm, $relation, $fields) {
        $query->orWhereHas($relation, function ($q) use ($searchTerm,$fields) {
            $q->where($fields[0], 'like', '%' . $searchTerm . '%');
            for($i = 1; $i < count($fields); $i++) {
                $q->orWhere($fields[$i], 'like', '%' . $searchTerm . '%');
            }
        });
    }
}
