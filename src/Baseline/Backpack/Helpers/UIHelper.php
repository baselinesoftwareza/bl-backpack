<?php


namespace Baseline\Backpack\Helpers;


use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UIHelper
{

    /**
     * Method adds a javascript snippet to be called when a datatable is refreshed
     * function() { .... } is not necessary, just fill in the dots
     *
     * @param $crud
     * @param $javascript
     */
    public static function addJsOnListDraw($crud, $javascript)
    {
        $crud->scriptlets[] = "\$(function() { \$('#crudTable').on('draw.dt',function() { $javascript }); })";
    }


    public static function makeListRowClickToAddress($crud, $pre_id_url, $post_id_url = "")
    {
        if (!Str::endsWith($pre_id_url, '/')) {
            $pre_id_url = $pre_id_url . "/";
        }
        if ($post_id_url && !Str::startsWith($post_id_url, '/')) {
            $post_id_url = "/" . $post_id_url;
        }
        self::makeListRowClickable($crud, "function() { location.href = '$pre_id_url' + id + '$post_id_url'; }");
    }

    public static function makeListRowClickable($crud, $function)
    {
        self::addJsOnListDraw($crud, view('bl-backpack.js.on_row_click', ['function' => $function]));
        $crud->addButtonFromView('line', 'row_click', 'row_click', 'end');
    }

    public static function addLoaderToSave($crud, $title = null, $text = null, $icon = null)
    {
        $crud->scriptlets[] = view('bl-backpack.js.add_loader_to_save', ['title' => $title, 'text' => $text, 'icon' => $icon]);
    }

    /**
     * @param $child
     * @return array
     */
    public static function buildButton($child): array
    {
        return [
            'icon' => MiscHelper::getNestedArrayValuesSafely($child, ['icon']) ?? 'la-cog',
            'basename' => MiscHelper::getNestedArrayValuesSafely($child, ['basename']),
            'type' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'type']) ?? 'inline',
            'label' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'label']) ?? MiscHelper::ucfirst_all($child['entity_name_strings'][1]),
            'priority' => MiscHelper::getNestedArrayValuesSafely($child, ['parent_index', 'priority']) ?? 10,
        ];
    }

    public static function getThresholdBadge(int|float|Carbon $value, int|float|Carbon $first_threshold, int|float|Carbon $second_threshold, $reverse = false)
    {
//        return self::getThresholdMarker($value, $first_threshold, $second_threshold,
//            $reverse ? 'badge-success' : 'badge-danger',
//            'badge-warning',
//            $reverse ? 'badge-danger' : 'badge-success');
        if(self::thresholdCompareLtEq($value,$first_threshold)) {
            return $reverse ? 'badge-danger' : 'badge-success';
        } else if (self::thresholdCompareLtEq($value,$second_threshold)) {
            return 'badge-warning';
        } else {
            return $reverse ? 'badge-success' : 'badge-danger';
        }
    }

    public static function getThresholdMarker(int|float|Carbon $value, int|float|Carbon $first_threshold, int|float|Carbon $second_threshold, $default_marker, $first_threshold_marker, $second_threshold_marker)
    {
        if (self::thresholdCompareLtEq($value, $first_threshold)) {
            return $first_threshold;
        } else if (self::thresholdCompareLtEq($value, $second_threshold)) {
            return $second_threshold;
        } else {
            return $default_marker;
        }
    }

    private static function thresholdCompareLtEq(int|float|Carbon $first, int|float|Carbon $second)
    {
        if ($first instanceof Carbon) {
            // same as saying $first <= $second
            return $second->isAfter($first);
        }
        return $first <= $second;
    }

    public static function mapBadge($value, $reverse_map, $default_badge = 'danger')
    {
        foreach ($reverse_map as $badge => $values) {
            if (is_array($values)) {
                if (in_array($value, $values)) {
                    return "badge-$badge";
                }
            } else {
                if ($values == $value) {
                    return "badge-$badge";
                }
            }
        }
        return $default_badge ? "badge-$default_badge" : "";
    }
    
    public static function isBootstrap5()
    {
        return config('backpack.ui.view_namespace_fallback') == 'backpack.theme-tabler::'
            || config('backpack.ui.view_namespace_fallback') == 'backpack.theme-coreuiv4::';
    }
    
    public static function isBootstrap4()
    {
        return config('backpack.ui.view_namespace_fallback') == 'backpack.theme-coreuiv2::';
    }
}
