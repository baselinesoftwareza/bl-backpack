<?php


namespace Baseline\Backpack\Helpers;


use CloudCreativity\LaravelJsonApi\Schema\SchemaProvider;

class JsonApiHelper
{
    static function getRelationship($include, $includeRelationships, $related_obj)
    {
        return [
            $include => [
                SchemaProvider::SHOW_SELF => false,
                SchemaProvider::SHOW_RELATED => true,
                SchemaProvider::SHOW_DATA => isset($includeRelationships[$include]),
                SchemaProvider::DATA => function () use ($related_obj) {
                    return $related_obj instanceof \Closure ? $related_obj() : $related_obj;
                }
            ]];
    }
}
