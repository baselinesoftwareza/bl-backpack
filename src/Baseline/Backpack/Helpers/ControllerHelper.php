<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 24/04/2020
 * Time: 23:19
 */

namespace Baseline\Backpack\Helpers;


use Baseline\Backpack\Helpers\ClassFinder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class ControllerHelper
{

    private static $controllerConfigs;
    private static $controllerConfigsByClass;
    private static $menu;

    /** This method builds the rootmap from the admin controllers
     *
     */
    public static function getControllerConfigs()
    {
        if (self::$controllerConfigs != null) {
            return self::$controllerConfigs;
        }
        // we have to build it
        $classfinder = new ClassFinder();
        $classes = $classfinder->getClassesByNamespace(array_merge(['App\Http\Controllers\Admin','App\Http\Controllers\Global'], array_map(fn($a) => 'App\\Integration\\' . $a . '\\Http\\Controllers\\Admin', config('bl-backpack.integration') ?? [])));
        $configs = [];
        foreach ($classes as $class) {
            if (isset($class::$crud_config) && !Str::endsWith($class, 'TemplateCrudController') && (!(new \ReflectionClass($class))->isAbstract())) {
                $configs[$class::$crud_config['basename']] = array_merge($class::$crud_config,
                    ['controller' => [
                        'class' => $class,
                        'class_name' => preg_replace('/.*\\\\/', '', $class)
                    ]
                    ]);
            }
        }
        $new_route_map = [];
        foreach ($configs as $basename => $crud_config) {
            $routeObj = $crud_config;
            $routeObj['route'] = $basename;
            if (!isset($routeObj['baseroute'])) {
                foreach (config('bl-backpack.integration') ?? [] as $integration) {
                    if (Str::startsWith($crud_config['controller']['class'], '\App\Integration\\' . $integration)) {
                        $routeObj['baseroute'] = config('backpack.base.route_prefix') . '/' . strtolower($integration);
                        continue;
                    }
                }
                $routeObj['baseroute'] ??= config('backpack.base.route_prefix');
            }
            $parent = key_exists('parent', $crud_config) ? $crud_config['parent'] : null;
            if ($parent) {
                while ($parent) {
                    $parent_config = $configs[$parent];
                    $routeObj['route'] = $parent . '/{' . $parent . '}/' . $routeObj['route'];
                    $parent = key_exists('parent', $parent_config) ? $parent_config['parent'] : null;
                }
            }
            $routeObj['full_route'] = $routeObj['baseroute'] . "/" . $routeObj['route'];
            $new_route_map[$basename] = $routeObj;
        }
        self::$controllerConfigs = $new_route_map;
        // Setup the by class lookup
        foreach ($new_route_map as $basename => $config) {
            self::$controllerConfigsByClass[$config['controller']['class']] = $config;
        }
        return self::$controllerConfigs;
    }

    private static $current_controller;

    static function getCurrentController()
    {
        return self::$current_controller;
    }

    static function setCurrentController($controller)
    {
        self::$current_controller = $controller;
    }

    static function loadControllerRoutes($baseroute)
    {
        foreach (self::getControllerConfigs() as $basename => $config) {
            if ($config['baseroute'] == $baseroute) {
                Route::crud($basename, $config['controller']['class_name']);
            }
        }
    }

    public static function getMenuConfig()
    {
        if (!self::$menu) {
            $menu = collect([
                'orphans' => [],
                'sections' => config('bl-backpack.menu.sections'),
            ])->recursive();
            $menu->get('sections')->each(function ($section) {
                $section->put('menuitems', collect([]));
            });
            $menu->get('orphans')->put('menuitems', collect([]));

            foreach (self::getControllerConfigs() as $basename => $config) {
                if ($model_menu = $config['menu'] ?? null) {
                    if ($model_menu['section'] ?? null) {
                        if ($section = $menu['sections']->where('name', $model_menu['section'])->first()) {
                            $section->get('menuitems')->push($config);
                        }
                    } else {
                        $menu->get('orphans')->get('menuitems')->push($config);
                    }
                }
            }
            // sort them by priority
            $menu->get('sections')->each(function ($section) {
                $section->put('menuitems', $section->get('menuitems')->sortBy('menu.priority'));
            });
            $menu->get('orphans')->put('menuitems', $menu->get('orphans')->get('menuitems')->sortBy('menu.priority'));
            self::$menu = $menu->toArray();
        }
        return self::$menu;
    }

    public static function getControllerConfigByBasename($basename)
    {
        return self::$controllerConfigs[$basename] ?? null;
    }

    public static function getControllerConfig($obj)
    {
        return self::$controllerConfigsByClass['\\' . get_class($obj)];
    }

    public static function getParentIndexLabelFromControllerConfig($config)
    {
        if ($value = MiscHelper::getNestedArrayValuesSafely($config, ['parent_index', 'label'])) {
            return $value;
        } else {
            return implode(" ", array_map('ucfirst', explode(" ", $config['entity_name_strings'][1])));
        }
    }

    /**
     * Adds the required SQL clauses to ensure that the scope is defined according to partner and route parameters
     *
     * @param $controller The controller running the route
     */
    public static function limitSearchAndEntries($controller)
    {
        $wheres = [];
        // If we are requesting global access and we are the super partner, we don't apply the partner_id limit
        if ($controller->global_access && TenantHelper::amISuperTenant()) {
            Log::info("Skipping tenant check for this route " . Route::currentRouteName() . ", global access is enabled and tenant is a super tenant");
        } else {
            // in every other circumstance we do (SECURITY!!)
            if ($controller->crud->basename == config('tenant.relation')) {
                // we can't add the standard here, we need to add the key, not the foreign key
                $wheres[$controller->crud->model->getTable() . '.id'] = session(config('tenant.foreign_key'));
            } else {
                // we need to check if this model is not a tenant model (must be explicitely stated)
                if(!property_exists($controller->crud->model,'is_non_tenant') || !$controller->crud->model->is_non_tenant) {
                    $wheres[$controller->crud->model->getTable() . '.' . (config('tenant.foreign_key'))] = session(config('tenant.foreign_key'));
                }
            }
        }
        $parameters = Route::current()->parameters();
        // Make sure all parameters are in the where clause
        //Log::debug(print_r($controller->ignore_parents,true));
        foreach ($parameters as $param => $value) {
            // we check if the column exists
            //  && Schema::hasColumn($controller->crud->model->getTable(),($param."_id"))
            if ($controller->basename != $param) {
                //$controller->crud->addClause('where', $param . '_id', $value);
                if ($controller->ignore_parents[$controller->basename][$param] ?? null) {
                    //Log::warning("We are not adding parent $param to security where clause because it is listed as being ignored on $controller->basename");
                } elseif (($controller->map_parents[$controller->basename][$param] ?? null) !== null) {
                    // map a parent to another foreign key
                    $wheres[$controller->map_parents[$controller->basename][$param] . "_id"] = $value;
                } else {
                    //Log::debug("Adding $controller->basename - ".$param." ".(debug_backtrace()[1]['function']));
                    $wheres[$param . '_id'] = $value;
                }
            }
        }
        if ($wheres) {
            $controller->crud->addClause('where', function ($query) use ($wheres) {
                foreach ($wheres as $field => $value) {
                    $query->where($field, $value);
                }
            });
        }
    }

    static function setupBackpackRoute($type, $routeName, $controller, $action, $item_operation = true)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::$type($config['route'] . ($item_operation ? '/{' . $routeName . '}' : '') . '/' . $action, [
            'as' => $routeName . '.' . $action,
            'uses' => $controller . '@' . $action,
            'operation' => $action,
        ]);
    }
}
