<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 09/05/2020
 * Time: 19:50
 */

namespace Baseline\Backpack\Helpers;

class BugFixHelper
{
    /**
     * @param $versioned
     * @return void
     */
    public static function fixVersionableSnapshotReversions($versioned): void
    {
        // there is a bug here with snapshotted versions and casted fields, they are stored badly and restored as raw arrays, where they should be raw json_encoded strings
        foreach ($versioned->getFillable() as $key) {
            // no raw value should be an array. If it is, we need to cast it back to the correct type
            $rawValue = ClassHelper::accessProtected($versioned, 'attributes')[$key];
            if (is_array($rawValue)) {
                // this allows casting to take effect
                $versioned->$key = $rawValue;
            }
        }
    }
}
