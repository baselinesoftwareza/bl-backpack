<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 25/04/2020
 * Time: 23:35
 */

namespace Baseline\Backpack\Controller;


use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait CrudSecurity
{
    public $ignore_parents = [];
    public $map_parents = [];

    public function ignoreParentsWithEntries($controller,$parent_name) {
        $controller->ignore_parents[$controller->basename][$parent_name] = 1;
    }

    public function mapParentToAlternateId($controller,$parent_name,$mapped_name) {
        $controller->map_parents[$controller->basename][$parent_name] = $mapped_name;
    }

    /**
     * checks if the current user has access to the route.
     */
    public function checkAccessOrFail()
    {
        // First check he has access to this partner
        $user = backpack_user();
        if (!$user) {
            Log::error("Failed to find user, denying access");
            abort(403);
        }
        // currently the user's partner id has to match his session partner_id
        if (!TenantHelper::userAccessToCurrentTenant($user)) {
            Log::error("User $user->email doesn't have access to tenant " . TenantHelper::currentTenantId() . ", denying access");
            abort(403);
        }
        // The partner has to be valid
        if(TenantHelper::currentTenantId()) {
            if (!TenantHelper::isValidTenant(TenantHelper::currentTenantId())) {
                Log::error("User $user->email cannot proceed to tenant " . TenantHelper::currentTenantId() . " as they are invalid, denying access");
                abort(403);
            }
        }
        // If he is a super user accessing the global baseroute, he is good
        if(preg_match('%^'.config('tenant.global_baseroute').'/%',request()->path())) {
            if(TenantHelper::amISuperTenant()) {
                return true;
            } else {
                Log::error("User $user->email is attempting to access the global area but he is not a super tenant");
                abort(403);
            }
        }

        // Checks whether current partner has access to the chain of objects
        // one at a time
        $known_basenames = collect(ControllerHelper::getControllerConfigs())->pluck('basename')->toArray();
        $parameters = [];
        foreach(Route::current()->parameters() as $key => $value) {
            if(in_array($key,$known_basenames)) {
                $parameters[$key] = $value;
            }
        }

        //Log::debug("Checking ".$user->email." permissions on ".request()->path().", ". count($parameters) . " parameters . ".json_encode($parameters));
        try {
            // If there are parameters to check
            if (count($parameters) > 0) {
                foreach ($parameters as $parameter => $value) {
                    //Log::debug("Checking user " . $user->email . ", tenant " . TenantHelper::currentTenantId() . " has access to $parameter -> " . $value);
                    // Get the model of the parameter
                    $modelType = ControllerHelper::getControllerConfigByBasename($parameter)['model'];
                    //Log::debug("Trying to access $modelType");
                    // Accessing of the partner model is forbidden
                    //      unless its under the global baseroute and you're the super partner
                    //      or its your own partner
                    if ($modelType == config('tenant.model') && !preg_match('|^'.config('tenant.global_baseroute').'/|', request()->path())) {
                        // trying to access another partner, no
                        if ($value != TenantHelper::currentTenantId()) {
                            Log::error("Access is denied, user " . $user->email . " is trying to access the partner model of $value, when its bound to ".TenantHelper::currentTenantId());
                            abort(403);
                        }
                        $model = new $modelType();
                        // If this parameter doesn't exist
                        if (!$model->where("id", $value)->first()) {
                            Log::error("Access is denied, user " . $user->email . " is not allowed to access $parameter $value, it doesn't seem to exist");
                            abort(403);
                        }

                    } else {
                        // we don't check tenant ownership on specific classes
                        if(property_exists($modelType,'is_non_tenant')) {
                            $model = new $modelType();
                            if($model->is_non_tenant) {
//                                Log::debug("Skipping tenant check for $parameter, it is a non tenant model");
                                continue;
                            }
                        }
                        // otherwise, we check our session's partner id matches the parameter we are trying to access.
                        if (!TenantHelper::checkCurrentTenantOwns($value,$modelType)) {
                            Log::error("Access is denied, user " . $user->email . " is not allowed to access $parameter $value, the tenant doesn't own that $parameter");
                            abort(403);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            Log::error("Failed to check permissions for " . $user->email . " on route " . request()->path() . " : " . $e->getMessage());
            abort(403);
        }
    }

}
