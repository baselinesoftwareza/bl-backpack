<?php

namespace Baseline\Backpack\Model;

use Mpociot\Versionable\Version;

class TenantedVersion extends Version
{

    public static function boot()
    {
        parent::boot();

        static::saving(function ($version) {
            if ($model_attributes = unserialize($version->model_data)) {
                $tenant_id_attr = config('tenant.foreign_key', 'tenant_id');
                if ($model_attributes[$tenant_id_attr] ?? null) {
                    $version->{$tenant_id_attr} = $model_attributes[$tenant_id_attr];
                }
            }
        });
    }
}
