<?php

namespace Baseline\Backpack\Model\Traits;

use Baseline\Backpack\Helpers\TenantHelper;
use Mpociot\Versionable\VersionableTrait;

trait TenantedVersionableTrait
{
    use VersionableTrait;

    // we need to fix this to use the current user id as recognized by the tenant/backpack system
    public function getAuthUserId()
    {
        return TenantHelper::currentUserId();
    }

}
