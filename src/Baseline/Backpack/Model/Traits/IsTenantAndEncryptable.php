<?php


namespace Baseline\Backpack\Model\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

trait IsTenantAndEncryptable
{
    use IsTenant { IsTenant::getAttribute as isTenantGetAttribute; }
    use Encryptable { Encryptable::getAttribute as encryptableGetAttribute; }

    function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        $decrypted = $this->encryptableGetAttribute($key);
        if($decrypted != $value) {
            return $decrypted;
        }
        $tenant = $this->isTenantGetAttribute($key);
        return $tenant;
    }
}
