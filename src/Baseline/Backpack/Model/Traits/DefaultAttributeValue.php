<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 04/12/2018
 * Time: 02:56
 */

namespace Baseline\Backpack\Model\Traits;


use Baseline\Backpack\Helpers\LogHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Support\Facades\Log;

trait DefaultAttributeValue
{
    /**
     * Please remember to add the attributes that need defaults
     * protected $default_values = [ 'attribute' => DEFAULT_VALUE|Closure ];
     */

     public static function bootDefaultAttributeValue() {

         static::creating(function($obj) {
             self::setDefaultValues($obj);
         });
         static::updating(function($obj) {
             self::setDefaultValues($obj);
         });

     }

    /**
     * @param $obj
     */
    private static function setDefaultValues($obj): void
    {
        if (isset($obj->default_values)) {
            foreach ($obj->default_values as $field => $default_value) {
                $obj->$field ??= $default_value instanceof \Closure ? $default_value() : $default_value;
            }
        }
    }
}
