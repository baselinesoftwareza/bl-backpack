<?php


namespace Baseline\Backpack\Model\Traits;


trait LocationTrait
{
    // remember to add these fields and location to the fillable
    
    protected $location_attributes = [
        'latitude' => 'latitude',
        'longitude' => 'longitude',
        'address' => 'address',
    ];

    protected function location(): \Illuminate\Database\Eloquent\Casts\Attribute
    {
        return \Illuminate\Database\Eloquent\Casts\Attribute::make(
            get: function ($value, $attributes) {
            return json_encode([
                'lat' => $attributes[$this->location_attributes['latitude']],
                'lng' => $attributes[$this->location_attributes['longitude']],
                'formatted_address' => preg_replace('/[\r\n]+/', ", ", $attributes[$this->location_attributes['address']] ?? '')
            ], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_THROW_ON_ERROR);
        },
            set: function ($value) {
            $location = json_decode($value);
            return [
                $this->location_attributes['latitude'] => $location->lat,
                $this->location_attributes['longitude'] => $location->lng,
                $this->location_attributes['address'] => preg_replace('/(,[\s]*)+/',"\n",$location->formatted_address ?? '')
            ];
        }
        );
    }
}
