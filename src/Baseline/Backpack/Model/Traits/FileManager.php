<?php


namespace Baseline\Backpack\Model\Traits;


use App\Models\Supplier;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait FileManager
{

    /*
     * To use this trait, create a protected variable as follows
     *
        protected $files = [
            'tax_clearance_cert' => ['disk' => 'supplier', 'meta_field' => 'tax_stuff', 'multiple' => true, 'keep' => true ],
            'bbbee_cert' => ['disk' => 'supplier'], // meta_field defaults to bbbee_cert_meta, multiple defaults to false, keep defaults to false
            'csd_report' => ['disk' => 'supplier'],
            'cidb_report' => ['disk' => 'supplier'],
        ];
     *
     * Then create the camel case file methods for uploading the files (it pulls the filename from the camelcase method)
     *
        public function setCsdReportAttribute($value)
        {
            $this->setFileValue($value);
        }
     *
     *  meta_field defaults to $key."_meta" from the array.  If this field exists in $casts, it will put meta data in there such as
     *  filename, mimetype and size which will be retrievable like
     *  $object->csd_report_meta['filename']
     *  $object->tax_stuff[0]['filename']
     *  etc.
     */

    /*
     * add the $files variable
     */

    public static function bootFileManager()
    {
        static::deleted(function ($obj) {
            if (property_exists($obj, 'files')) {
                foreach ($obj->files as $name => $info) {
                    $info = $obj->fmGetFileInfo($name);
                    if(!$info['keep']) {
                        if ($info['multiple']) {
                            if ($files = $obj->{$name}) {
                                foreach($files as $file) {
                                    Storage::disk($info['disk'])->delete($file['filename_in_storage']);
                                }
                            }
                        } else {
                            if ($obj->{$name}) {
                                Storage::disk($info['disk'])->delete($obj->{$name});
                            }
                        }
                    }
                }
            }
        });
    }

    public function setFileValue($value, $attribute_name = null, $path = "")
    {
        // auto detect attribute name
        if (!$attribute_name) $attribute_name = Str::snake(preg_replace('/set(.*)?Attribute/', '$1', debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function']));

        if ($fileinfo = $this->fmGetFileInfo($attribute_name)) {
            $meta = $fileinfo['meta_field'];
            $disk = $fileinfo['disk'];
            $multiple = $fileinfo['multiple'] ?? false;
            $destination_path = "/" . TenantHelper::currentTenantId() . ($path ? (Str::startsWith($path, "/") ? $path : "/$path") : "");

            $meta_value = null;
            $has_meta = false;
            if ($this->casts[$meta] ?? null) {
                $meta_value = $this->{$meta};
                $has_meta = true;
            }

            // now fork to single or multiple
            if ($multiple) {
                $meta_value = $this->setMultipleFileValue($attribute_name, $disk, $destination_path, $meta_value, !$fileinfo['keep']);
            } else {
                $meta_value = $this->setSingleFileValue($value, $attribute_name, $disk, $destination_path);
            }
            if ($has_meta) {
                $this->{$meta} = $meta_value ?? [];
            }
        }
    }

    private function setSingleFileValue($value, $attribute_name, $disk, $destination_path, $delete = true)
    {
        if (request()->hasFile($attribute_name)) {
            $file = request()->file($attribute_name);

            $meta = [
                'filename' => $file->getClientOriginalName(),
                'mimetype' => $file->getMimeType() ?? '',
                'size' => $file->getSize()
            ];

            //$this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
            // if a new file is uploaded, delete the file from the disk
            if (request()->hasFile($attribute_name) &&
                $this->{$attribute_name} &&
                $this->{$attribute_name} != null) {
                if($delete) {
                    \Storage::disk($disk)->delete($this->{$attribute_name});
                }
                $this->attributes[$attribute_name] = null;
            }

            // if the file input is empty, delete the file from the disk
            if (is_null($value) && $this->{$attribute_name} != null) {
                if($delete) {
                    \Storage::disk($disk)->delete($this->{$attribute_name});
                }
                $this->attributes[$attribute_name] = null;
            }

            // if a new file is uploaded, store it on disk and its filename in the database
            if (request()->hasFile($attribute_name) && request()->file($attribute_name)->isValid()) {
                // 1. Generate a new file name
                $file = request()->file($attribute_name);
                $new_file_name = md5($file->getClientOriginalName().random_int(1, 9999).time()).'.'.$file->getClientOriginalExtension();

                // 2. Move the new file to the correct path
                $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

                // 3. Save the complete path to the database
                $this->attributes[$attribute_name] = $file_path;
            }


            return $meta;
        }
        return null;
    }


    private function setMultipleFileValue($attribute_name, $disk, $destination_path, $meta_value, $delete = true)
    {
        $meta_value = $meta_value ?? [];

        // the below is lifted from HasFileUploads trait from backpack
        if (!is_array($this->{$attribute_name})) {
            $attribute_value = json_decode($this->{$attribute_name}, true) ?? [];
        } else {
            $attribute_value = $this->{$attribute_name};
        }
        $files_to_clear = request()->get('clear_' . $attribute_name);

        // if a file has been marked for removal,
        // delete it from the disk and from the db
        if ($files_to_clear) {
            foreach ($files_to_clear as $key => $filename) {
                if($delete) {
                    \Storage::disk($disk)->delete($filename);
                }
                $attribute_value = Arr::where($attribute_value, function ($value, $key) use ($filename) {
                    return $value != $filename;
                });
                $meta_value = Arr::where($meta_value, function ($value, $key) use ($filename) {
                    return $value['filename_in_storage'] != $filename;
                });
            }
        }

        // if a new file is uploaded, store it on disk and its filename in the database
        if (request()->hasFile($attribute_name)) {
            foreach (request()->file($attribute_name) as $file) {
                if ($file->isValid()) {
                    if ($file->isValid()) {
                        // 1. Generate a new file name
                        $new_file_name = md5($file->getClientOriginalName() . random_int(1, 9999) . time()) . '.' . $file->getClientOriginalExtension();

                        // 2. Move the new file to the correct path
                        $file_path = $file->storeAs($destination_path, $new_file_name, $disk);

                        // 3. Add the public path to the database
                        $attribute_value[] = $file_path;

                        // 4. Update the meta info
                        $meta_value[] = [
                            'filename_in_storage' => $file_path,
                            'filename' => $file->getClientOriginalName(),
                            'mimetype' => $file->getMimeType() ?? '',
                            'size' => $file->getSize()
                        ];
                    }
                }
            }
        }

        $this->attributes[$attribute_name] = json_encode($attribute_value);
        return $meta_value;
    }

    function fmGetFileInfo($file_attribute,$filename = "") {
        if (property_exists($this, 'files') && $file_info = $this->files[$file_attribute] ?? null) {
            $file_info['meta_field'] = $file_info['meta_field'] ?? $file_attribute . '_meta';
            $file_info['multiple'] = $file_info['multiple'] ?? false;
            $file_info['keep'] = $file_info['keep'] ?? false;
            if (!($file_info['multiple'])) {
                $file_info['file_meta'] = $this->{$file_info['meta_field']};
            } else {
                if($filename) {
                    foreach (($this->{$file_info['meta_field']} ?? []) as $file_meta) {
                        if ($file_meta['filename_in_storage'] == $filename) {
                            $file_info['file_meta'] = $file_meta;
                            break;
                        }
                    }
                }
            }
            return $file_info;
        }
        return null;
    }

    /*
    function fmGetSingleFileInfo($file_attribute)
    {
        if (property_exists($this, 'files') && $file_info = $this->files[$file_attribute] ?? null) {
            if (!($file_info['multiple'] ?? false)) {
                $file_info['meta_field'] = $file_info['meta_field'] ?? $file_attribute . '_meta';
                return $file_info;
            }
        }
        return null;
    }

    function fmGetMultipleFileInfo($multiple_file_attribute, $filename)
    {
        //Log::debug(print_r($this->files,true));
        //Log::debug("$multiple_file_attribute,$filename");
        if (property_exists($this, 'files') && $file_info = $this->files[$multiple_file_attribute] ?? null) {
            if ($file_info['multiple'] ?? false) {
                $file_info['meta_field'] = $file_info['meta_field'] ?? $multiple_file_attribute . '_meta';
                //Log::debug(print_r($this->{$file_info['meta_field']},true));
                foreach (($this->{$file_info['meta_field']} ?? []) as $file_meta) {
                    if ($file_meta['filename_in_storage'] == $filename) {
                        $file_info['file_meta'] = $file_meta;
                        return $file_info;
                    }
                }
            }
        }
        return null;
    }
    */

}
