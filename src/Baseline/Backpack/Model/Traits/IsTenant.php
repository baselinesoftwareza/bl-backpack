<?php


namespace Baseline\Backpack\Model\Traits;


use Baseline\Backpack\Helpers\DebugHelper;
use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait IsTenant
{


    public static function bootIsTenant() {
        // Adds the relation dynamically
        Builder::macro(config('tenant.relation'), function () {
            return $this->getModel()->belongsTo(config('tenant.model'),config('tenant.foreign_key'));
        });
        // adds the global scope if this route contains the tenant middleware
        if(in_array('tenant', Route::current()?->middleware() ?? [])) {
            $table = (new self())->table;
            static::addGlobalScope('tenant', function (Builder $builder) use ($table) {
                $builder->where($table.'.'.config('tenant.foreign_key'), TenantHelper::currentTenantId());
            });
        }
    }

    // updates fillable with the tenant id foreign key
    public function __construct(array $attributes = [])
    {
        $this->addTenantIdToFillable();
        parent::__construct($attributes);
    }

    public function addTenantIdToFillable()
    {
        if (!in_array(config('tenant.foreign_key'), $this->fillable)) {
            $this->fillable[] = config('tenant.foreign_key');
        }
    }
    public function getAttribute($key) {
        $method_name = config('tenant.relation');
        if($key == $method_name) {
            return $this->$method_name()->first();
        }
        return parent::getAttribute($key);
    }

}
