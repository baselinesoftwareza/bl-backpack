<?php


namespace Baseline\Backpack\Build;


use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;

class CreateTool
{

    public static function createMigration($table, $fields, $ancestors)
    {
        $date = Carbon::now()->format("Y_m_d");
        $filename = $date . "_000000_create_" . $table . "_table.php";
        $data = [
            'class' => 'Create' . implode('', array_map(function ($a) {
                    return Str::ucfirst($a);
                }, explode('_', $table))) . 'Table',
            'table' => $table,
            'fields' => $fields,
            'ancestors' => $ancestors
        ];
        $path = base_path('database/migrations/' . $filename);
        self::writeViewToFile('migration', $data, $path);
        return $path;
    }

    public static function createModel($table, $model, $fields, $ancestors)
    {
        $data = [
            'model' => $model,
            'table' => $table,
            'fields' => $fields,
            'ancestors' => $ancestors,
        ];
        $filename = $model . ".php";
        $path = base_path('app/Models/' . $filename);

        self::writeViewToFile('model', $data, $path);
        return $path;
    }

    public static function createTransformer($model, $fields, $name = null, $namespace = 'App\Http\Transformers')
    {
        $namespace ??= 'App\Http\Transformers';
        $name = $name ?? $model . 'Transformer';
        $data = [
            'model' => $model,
            'fields' => $fields ?? [],
            'class' => $name,
            'namespace' => $namespace
        ];
        $filename = "$name.php";
        $basepath = preg_replace('/^App/', 'app', preg_replace('/\\\\/', '/', $namespace));
        $path = base_path($basepath . '/' . $filename);

        self::writeViewToFile('transformer', $data, $path);
        return $path;
    }

    public static function createController(
        $model,
        $basename,
        $name_singular,
        $name_plural,
        $operations,
        $fields,
        $ancestors,
        $route_parent,
        $baseroute
    )
    {
        $data = [
            'model' => $model,
            'basename' => $basename,
            'name_singular' => $name_singular,
            'name_plural' => $name_plural,
            'operations' => $operations,
            'fields' => $fields,
            'ancestors' => $ancestors,
            'route_parent' => $route_parent,
            'baseroute' => $baseroute,
        ];
        $filename = $model . "Controller.php";
        $path = base_path('app/Http/Controllers/Admin/' . $filename);

        self::writeViewToFile('controller', $data, $path);
        return $path;
    }

    public static function createRequest($model, $fields, $ancestors, $table, $route_parent)
    {
        $filename = $model . "Request.php";
        $data = [
            'model' => $model,
            'fields' => $fields,
            'ancestors' => $ancestors,
            'table' => $table,
            'route_parent' => $route_parent,
        ];
        $path = base_path('app/Http/Requests/Admin/' . $filename);
        self::writeViewToFile('request', $data, $path);
        return $path;
    }

    private static function writeViewToFile($view, $data, $file_path)
    {
        $data['tenant'] = config('tenant');
        View::addLocation(__DIR__ . '/../../../../Templates/views');
        View::addNamespace('bl-backpack', __DIR__ . '/../../../../Templates/views');
        $php = view('bl-backpack::' . $view, $data);
        if (!file_exists(dirname($file_path))) {
            mkdir(dirname($file_path), 0755, true);
        }
        file_put_contents($file_path, $php);
    }
}
