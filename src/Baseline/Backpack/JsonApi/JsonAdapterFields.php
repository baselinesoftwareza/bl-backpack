<?php

namespace Baseline\Backpack\JsonApi;

use Baseline\Backpack\Helpers\MiscHelper;
use Illuminate\Support\Facades\Log;

trait JsonAdapterFields
{
    protected function getFillable($record)
    {
        if ($record->id) {
            $fillable = $this->getUpdatingFillable();
        } else {
            $fillable = $this->getCreatingFillable();
        }
        return $fillable;
    }

    public function getUpdatingFillable()
    {
        $fillable = [];
        $fields = $this->getFields();
        foreach (array_keys($fields) as $field) {
            if (MiscHelper::getNestedArrayValuesSafely($fields[$field], ['update', 'write'])
                || MiscHelper::getNestedArrayValuesSafely($fields[$field], ['createupdate', 'write'])
            ) {
                $fillable[] = $field;
            }
        }
        return $fillable;
    }

    public function getCreatingFillable()
    {
        $fillable = [];
        $fields = $this->getFields();
        foreach (array_keys($fields) as $field) {
            if (MiscHelper::getNestedArrayValuesSafely($fields[$field], ['create', 'write'])
                || MiscHelper::getNestedArrayValuesSafely($fields[$field], ['createupdate', 'write'])
            ) {
                $fillable[] = $field;
            }
        }
        return $fillable;
    }


}
