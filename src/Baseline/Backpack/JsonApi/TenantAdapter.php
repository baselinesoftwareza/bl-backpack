<?php

namespace Baseline\Backpack\JsonApi;

use App\JsonApi\JsonAdapterFields;
use Baseline\Backpack\Helpers\TenantHelper;
use Baseline\Backpack\Scopes\EnabledTenantScope;
use CloudCreativity\LaravelJsonApi\Pagination\StandardStrategy;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class TenantAdapter extends \CloudCreativity\LaravelJsonApi\Eloquent\AbstractAdapter
{
    use JsonAdapterFields;

    public function __construct(Model               $model,
                                StandardStrategy    $paging,
    )
    {
        parent::__construct($model, $paging);
        $this->addScopes(new EnabledTenantScope());
    }

    /**
     * @param Builder $query
     * @param Collection $filters
     * @return void
     */
    protected function filter($query, Collection $filters)
    {
        $this->filterWithScopes($query, $filters);
    }

    public function creating($object) {
        $object->{config('tenant.foreign_key')} = TenantHelper::currentTenantId();
    }
}
