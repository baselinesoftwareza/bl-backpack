<?php

namespace Baseline\Backpack\Commands;

use App\Models\Tenant;
use App\Models\User;
use Baseline\Backpack\Build\CreateTool;
use Baseline\Backpack\Helpers\MiscHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class CreateMvc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:create_mvc {table} {name_plural} {operations_comma_delimited} {ancestors_comma_delimited} {fields_comma_delimited} {route_parent} {baseroute?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a migration,model,controller and request for an object e.g. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $table = $this->argument("table");
        $model = MiscHelper::underscoresToCamelCase($table,true);
        $route_basename = $table;
        $name_singular = strtolower(MiscHelper::underscoresToSpaceUcfirst($table));
        $name_plural = strtolower($this->argument("name_plural"));
        $operations = $this->argument("operations_comma_delimited") ? explode(",",$this->argument("operations_comma_delimited")) : [];
        $fields = $this->argument("fields_comma_delimited") ? explode(",",$this->argument("fields_comma_delimited")) : [];
        $ancestors = $this->argument("ancestors_comma_delimited") ? explode(",",$this->argument("ancestors_comma_delimited")) : [];
        $route_parent = strtolower($this->argument("route_parent"));
        $baseroute = strtolower($this->argument("baseroute"));

        $filename = CreateTool::createMigration($table,$fields,$ancestors);
        print("Created migration ".$filename."\n");
        $filename = CreateTool::createModel($table,$model,$fields,$ancestors);
        print("Created model ".$filename."\n");
        $filename = CreateTool::createController($model,$route_basename,$name_singular,$name_plural,$operations,$fields,$ancestors,$route_parent,$baseroute);
        print("Created controller ".$filename."\n");
        $filename = CreateTool::createRequest($model,$fields,$ancestors,$table,$route_parent);
        print("Created request ".$filename."\n");
        print("!!!!NB!!!! run composer dump-autoload\n");

    }
}
