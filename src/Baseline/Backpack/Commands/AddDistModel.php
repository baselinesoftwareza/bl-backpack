<?php

namespace Baseline\Backpack\Commands;

use Illuminate\Console\Command;

class AddDistModel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:add_model {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the predefined backpack model (e.g. User)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->argument('model');
        // Controller
        $prefix = __DIR__."/../../../app/Models/";
        if(file_exists($prefix.$model.".php")) {
            $dst_prefix = base_path('app/Models/');
            if(!is_dir($dst_prefix)){
                mkdir($dst_prefix, 0755, true);
            }
            copy($prefix.$model.".php", $dst_prefix.$model.".php");
            print("Created ".$dst_prefix.$model.".php\n");
        } else {
            print("No such model ".$prefix.$model.".php\n");
            die(255);
        }
    }
}
