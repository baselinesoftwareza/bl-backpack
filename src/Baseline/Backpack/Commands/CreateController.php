<?php

namespace Baseline\Backpack\Commands;

use App\Models\Tenant;
use App\Models\User;
use Baseline\Backpack\Build\CreateTool;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class CreateController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bl-backpack:create_controller {model} {basename} {name_singular} {name_plural} {operations_comma_delimited} {ancestors_comma_delimited} {fields_comma_delimited} {route_parent} {baseroute?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a controller for a model e.g. ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->argument("model");
        $basename = strtolower($this->argument("basename"));
        $name_singular = strtolower($this->argument("name_singular"));
        $name_plural = strtolower($this->argument("name_plural"));
        $operations = $this->argument("operations_comma_delimited") ? explode(",",$this->argument("operations_comma_delimited")) : [];
        $fields = $this->argument("fields_comma_delimited") ? explode(",",$this->argument("fields_comma_delimited")) : [];
        $ancestors = $this->argument("ancestors_comma_delimited") ? explode(",",$this->argument("ancestors_comma_delimited")) : [];
        $route_parent = strtolower($this->argument("route_parent"));
        $baseroute = strtolower($this->argument("baseroute"));

        $filename = CreateTool::createController(
            $model,
            $basename,
            $name_singular,
            $name_plural,
            $operations,
            $fields,
            $ancestors,
            $route_parent,
            $baseroute);

        print("Created controller ".$filename."\n");
        print("NB!!!! run composer dump-autoload\n");

    }
}
