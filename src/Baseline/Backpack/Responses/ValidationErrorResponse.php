<?php


namespace Baseline\Backpack\Responses;


use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Validation\Validator;

class ValidationErrorResponse extends ApiResponse implements Jsonable
{
    public function __construct(Validator $validator)
    {
        $errors = [];
        foreach ($validator->errors()->toArray() as $field => $messages) {
            foreach($messages as $message) {
                $myerror = ['property' => $field, 'message' => $message];
                $errors[] = $myerror;
            }
        }
        $this->json['errors'] = $errors;
    }


}
