<?php


namespace Baseline\Backpack\Responses;


class ForbiddenResponse extends ErrorResponse
{
    public function __construct($message, $technical_message = null)
    {
        parent::__construct($message, $technical_message, 403);
    }
}
