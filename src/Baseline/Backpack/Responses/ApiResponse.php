<?php


namespace Baseline\Backpack\Responses;


use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Responsable;

abstract class ApiResponse implements Jsonable, Responsable
{
    protected $json = [];
    protected $status;

    public function toJson($options = 0)
    {
        $json = json_encode((object)array_filter($this->json));
        return $json;
    }
    
    public function toResponse($request)
    {
        return response()->json($this,$this->status);
    }
}
