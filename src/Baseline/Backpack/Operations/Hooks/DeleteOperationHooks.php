<?php


namespace Baseline\Backpack\Operations\Hooks;


interface DeleteOperationHooks
{
    function before_destroy($id);
    function after_destroy($id, $result, bool $success, ?\Exception $exception);
}
