<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Transformers\BaseTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait FieldEditOperation {
    use CrudSecurity;

    public abstract function getTransformer();

    /**
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupFieldEditRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::post($config['route'].'/{'.$routeName.'}/field_edit', [
            'as'        => $routeName.'.field_edit',
            'uses'      => $controller.'@field_edit',
            'operation' => 'field_edit',
        ]);
        Route::post($config['route'].'/{'.$routeName.'}/field_edit2', [
            'as'        => $routeName.'.field_edit2',
            'uses'      => $controller.'@field_edit2',
            'operation' => 'field_edit',
        ]);
    }

    protected function setupFieldEditDefaults()
    {
        $this->crud->allowAccess('dashboard');

        $this->crud->operation(['list','dashboard'], function () {
//            $this->crud->scripts[] = 'bl-backpack/js/field_edit.js';
            $this->addScript('bl-backpack/js/field_edit.js');
            $this->addStyleSheet('bl-backpack/css/field_edit.css');
        });
        $this->crud->operation(['field_edit'], function() {
            // we need the column definitions
            $this->setupListOperation();
            $this->setupUpdateOperation();
        });
    }

    public function field_edit() {
        // Check access
        $this->checkAccessOrFail();
        // validate
        if($formRequestClass = $this->crud->getFormRequest()) {
            // we don't want the form request
            $this->crud->setFormRequest(null);
            // replace it with individual rule
            $formRequest = new $formRequestClass();
            $rules = $formRequest->rules();
            $messages = $formRequest->messages();
            $field = request()->input('field');
            if ($rules[$field] ?? null) {
                $this->crud->setValidation([$field => $rules[$field]],$messages);
            }
        }
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        // register any Model Events defined on fields
        $this->crud->registerFieldEvents();
        // redirect
        ClassHelper::callIfExists($this,'before_field_update',$this->getId());
        $result = null;
        try {
            // update the row in the db
            $item = $this->crud->update(
                $this->getId(),
                $request->only($request->input('field')),
            );
            $this->data['entry'] = $this->crud->entry = $item;
            $result = fractal($item,$this->getTransformer())->toArray();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_field_update',$this->getId(),null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_field_update',$this->getId(),$result,$this->isSuccessful($result),null)
            ?? $result;
    }

    public function field_edit2() {
        // Check access
        $this->checkAccessOrFail();
        // validate
        if($formRequestClass = $this->crud->getFormRequest()) {
            // we don't want the form request
            $this->crud->setFormRequest(null);
            // replace it with individual rule
            $formRequest = new $formRequestClass();
            $rules = $formRequest->rules();
            $messages = $formRequest->messages();
            $field = request()->input('field');
            if ($rules[$field] ?? null) {
                $this->crud->setValidation([$field => $rules[$field]],$messages);
            }
        }
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();
        // register any Model Events defined on fields
        $this->crud->registerFieldEvents();
        // redirect
        ClassHelper::callIfExists($this,'before_field_update',$this->getId());
        $result = null;
        try {
            $original_item = $this->crud->getCurrentEntry();
            // update the row in the db
            $item = $this->crud->update(
                $this->getId(),
                $request->only($request->input('field')),
            );
            // switch to datatables, get single row back
            $result = $this->crud->getEntriesAsJsonForDatatables([$item], 1, 1, 0);
            $oldFormattedEntry = $this->crud->getEntriesAsJsonForDatatables([$original_item], 1, 1, 0);
            //loop through the columns and find the changed ones
            $changedColumns = [];
            for($i = 0; $i < count($result['data'][0]); $i++) {
                if($result['data'][0][$i] != $oldFormattedEntry['data'][0][$i]) {
                    $changedColumns[] = $i;
                }
            }
            $result['changed_columns'] = $changedColumns;
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_field_update',$this->getId(),null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_field_update',$this->getId(),$result,$this->isSuccessful($result),null)
            ?? $result;
    }
}
