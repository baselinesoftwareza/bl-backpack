<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use App\Helpers\Networking;
use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\BugFixHelper;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Baseline\Backpack\Model\Traits\TenantedVersionableTrait;
use Baseline\Backpack\Transformers\BaseTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait JsonFetchOperation
{
    use CrudSecurity;

    protected $json_fetch_entry_transformer = null;

    /**
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupJsonFetchRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'] . '/{' . $routeName . '}/json_fetch', [
            'as' => $routeName . '.json_fetch_on_id',
            'uses' => $controller . '@json_fetch',
            'operation' => 'json_fetch_on_id',
        ]);
        Route::get($config['route'] . '/{' . $routeName . '}/json_fetch_entry', [
            'as' => $routeName . '.json_fetch_entry',
            'uses' => $controller . '@json_fetch_entry',
            'operation' => 'json_fetch_entry',
        ]);
        Route::get($config['route'] . '/json_fetch', [
            'as' => $routeName . '.json_fetch',
            'uses' => $controller . '@json_fetch',
            'operation' => 'json_fetch',
        ]);
    }

    public function isVersionable()
    {
        return in_array(TenantedVersionableTrait::class, class_uses($this->crud->getModel()));
    }

    public function json_fetch_entry()
    {
        $this->checkAccessOrFail();
        if ($transformer = $this->json_fetch_entry_transformer) {
            $entry = $this->crud->getCurrentEntry();
            $transformer = new $transformer();
            $_latest = fractal($entry, $transformer)->toArray();
            if ($this->isVersionable()) {
                if (request()->input('version_id')) {
                    $versioned = $entry->getVersionModel(request()->input('version_id'));
                    $result = fractal($versioned, $transformer)->toArray();
                    $result['_latest'] = $_latest;
                    $result['_version_id'] = request()->input('version_id');
                } else {
                    $result = $_latest;
                    $current_version = $entry->currentVersion();
                    $result['_version_user'] = $current_version?->responsibleUser?->name;
                    $result['_version_date'] = $current_version?->created_at ?? $entry->updated_at;
                }
                foreach ($entry->versions()->orderBy('created_at', 'DESC')->limit(20)->get() as $version) {
                    $result_version = $transformer->smap([
                        'created_at',
                    ], $version);
                    $result_version['id'] = $version->version_id;
                    $result_version['user'] = $version->responsibleUser?->name;
                    $result['_versions'][] = $result_version;
                    if ($version->version_id == request()->input('version_id')) {
                        $result['_version_user'] = $version->responsibleUser?->name;
                        $result['_version_date'] = $version->updated_at;
                    }
                }
            } else {
                $result = $_latest;
            }
            return $result;
        }
        // by default we don't allow this
        abort(403);
    }

    public function json_fetch()
    {
        // Check access
        $this->checkAccessOrFail();
        // get the relationship we are fetching
        $relationship = "json_fetch_" . request()->input('fetch');
        if (!method_exists($this, $relationship)) {
            Log::error("Failed to fetch json from a relationship which doesn't exist [" . get_class($this) . "->$relationship]");
            abort(404);
        }
        // redirect
        ClassHelper::callIfExists($this, 'before_json_fetch', $this->getId());
        try {

            $result = new JsonResponse($this->$relationship());
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_json_fetch', $this->getId(), null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_json_fetch', $this->getId(), $result, $this->isSuccessful($result), null)
            ?? $result;
    }
}
