<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

trait DeleteOperation
{
    use CrudSecurity;

    protected $veto_relationships = [];

    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation {
        destroy as backpackDestroy;
    }

    /**
     * We need to override this method because it uses {id} as a parameter name, this doesn't work for nested parameters
     * Check this everytime for updated methods
     *
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupDeleteRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::delete($config['route'].'/{'.$routeName.'}', [
            'as'        => $routeName.'.destroy',
            'uses'      => $controller.'@destroy',
            'operation' => 'delete',
        ]);
    }

    public function destroy($id) {
        // Check access
        $this->checkAccessOrFail();
        // check vetos
        if(($return = $this->checkVetos()) !== null) return $return;
        // Remember, the $id passed in here is wrong for nested routes, so we replace it
        $id = $this->crud->getCurrentEntryId();
        // we need to make sure there are no veto relationships, i.e. relationships which can stop a delete
        if($this->vetodByRelationship($id)) {
            Log::error("Some relationships veto'd the deletion of this element $id of ".$this->basename);
            return 0;
        }
        // we don't need to further check to match $id, because there is only one from the route
        // Run the backpack update
        ClassHelper::callIfExists($this,'before_destroy',$id);
        try {
            $result = $this->backpackDestroy($id);
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this,'after_destroy',$id,null,false,$e);
            throw $e;
        }
        return ClassHelper::callIfExists($this,'after_destroy',$id,$result,$this->isSuccessful($result),null)
        ?? $result;
    }

    protected function addRelationshipVeto($relationshipName) {
        $model = $this->crud->model;
        if(method_exists($model,$relationshipName)) {
            $this->veto_relationships[] = $relationshipName;
        } else {
            Log::error("Relationship $relationshipName does not exist on $this->basename, cannot add a veto");
            throw new \Exception("Relationship $relationshipName does not exist on $this->basename, cannot add a veto");
        }
    }

    private function vetodByRelationship($id) {
        $model = $this->crud->model;
        $entry = $model::findOrFail($id);
        foreach($this->veto_relationships as $relationshipName) {
            if($entry->$relationshipName()->count() > 0) {
                Log::error("Vetoing deletion of $this->basename $id because '$relationshipName' relationships exist");
                return true;
            }
        }
        return false;
    }

}
