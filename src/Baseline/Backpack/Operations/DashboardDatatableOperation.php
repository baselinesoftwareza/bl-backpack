<?php

namespace Baseline\Backpack\Operations;

use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Route;

trait DashboardDatatableOperation
{
    use CrudSecurity;

    public abstract function getRoutes(): array;

    protected function setupDashboardDatatableRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        foreach ($this->getRoutes() as $route => $settings) {
            $uri = $config['route'] . '/{' . $routeName . '}/' . $route;
            $route_data = [
                'as' => $routeName . '.' . $route,
                'uses' => $controller . '@' . ($settings['uses'] ?? $route),
                'operation' => $route,
            ];
            switch ($settings['method'] ?? 'GET') {
                case 'GET':
                {
                    Route::get($uri, $route_data);
                    break;
                }
                case 'POST':
                {
                    Route::POST($uri, $route_data);
                    break;
                }
                default:
                    throw new \Exception("Failed to get method of dashboard datatable operation");
            }
        }
    }

    protected function setupDashboardDatatableDefaults()
    {
        $this->crud->allowAccess('dashboard_datatable');
        $this->crud->operation('dashboard', function () {

//            $this->crud->scripts[] = '/packages/datatables.net/js/jquery.dataTables.min.js';
//            $this->crud->scripts[] = '/packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js';
//            $this->crud->scripts[] = '/packages/datatables.net-responsive/js/dataTables.responsive.min.js';
//            $this->crud->scripts[] = '/packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js';
//            $this->crud->scripts[] = '/packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js';
//            $this->crud->scripts[] = '/packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js';

            $this->crud->scripts[] = 'https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js';
            $this->crud->scripts[] = 'https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap4.min.js';
            $this->crud->scripts[] = 'https://cdn.datatables.net/responsive/2.5.0/js/dataTables.responsive.min.js';
            $this->crud->scripts[] = 'https://cdn.datatables.net/fixedheader/3.4.0/js/dataTables.fixedHeader.min.js';
            $this->crud->scripts[] = '/bl-backpack/js/dashboard_datatable.js';
            $this->crud->scripts[] = '/bl-backpack/js/field_edit.js';


            $this->crud->stylesheets[] = 'https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap4.min.css';
            $this->crud->stylesheets[] = 'https://cdn.datatables.net/fixedheader/3.4.0/css/fixedHeader.dataTables.min.css';
            $this->crud->stylesheets[] = 'https://cdn.datatables.net/responsive/2.5.0/css/responsive.dataTables.min.css';

//            $this->crud->stylesheets[] = '/packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css';
//            $this->crud->stylesheets[] = '/packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css';
//            $this->crud->stylesheets[] = '/packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css';
        });
    }
}
