<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 26/04/2020
 * Time: 00:30
 */

namespace Baseline\Backpack\Operations;


use Baseline\Backpack\Controller\BaselineCoreController;
use Baseline\Backpack\Controller\CrudSecurity;
use Baseline\Backpack\Helpers\ClassHelper;
use Baseline\Backpack\Helpers\ControllerHelper;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Prologue\Alerts\Facades\Alert;

trait CreateOperation
{
    use CrudSecurity;
    use BaselineCoreController;

    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as backpackStore;
        create as backpackCreate;
        setupCreateDefaults as backpackSetupCreateDefaults;
    }

    /**
     * We need to create our own here because theirs doesn't cater for nested routes
     *
     * @param $segment
     * @param $routeName
     * @param $controller
     */
    protected function setupCreateRoutes($segment, $routeName, $controller)
    {
        $config = ControllerHelper::getControllerConfigByBasename($routeName);
        Route::get($config['route'] . '/create', [
            'as' => $routeName . '.create',
            'uses' => $controller . '@create',
            'operation' => 'create',
        ]);

        Route::post($config['route'], [
            'as' => $routeName . '.store',
            'uses' => $controller . '@store',
            'operation' => 'create',
        ]);
    }

    public function setupCreateDefaults()
    {
        // we need to add the parent fields
        //$this->addParentFields();
        // call the trait method
        $this->backpackSetupCreateDefaults();
    }

    public function create()
    {
        // Check access
        $this->checkAccessOrFail();
        // Save the return address if we need to
        $this->saveReturnAddress();
        // Stop the ui from printing "Back to list"
        $this->crud->denyAccess('list');
        // Prep the javascript helpers for the various fields/options
        $this->prepareJavascriptHelpers();
        // Set the heading
        if (!$this->crud->getHeading()) $this->crud->setHeading("Add " . ucwords($this->crud->entity_name));
        // Set the subheading
        if (!$this->crud->getSubHeading()) $this->crud->setSubHeading($this->subheading ?? 'Create');
        // Set the breadcrumbs
        $this->data['breadcrumbs'] = $this->getBreadcrumbs();
        // Run the backpack create
        ClassHelper::callIfExists($this, 'before_create');
        try {
            $result = $this->backpackCreate();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_create', null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_create', $result, true, null)
            ?? $result;
    }

    public function store()
    {
        // can't check perms on creating a partner
        if ($this->basename != config('tenant.relation')) {
            // Check access
            $this->checkAccessOrFail();
            // Add all of our parent parameters to our update to ensure that they are correctly set
            $this->mergeParents();
        }
        // check vetos
        if(($return = $this->checkVetos()) !== null) return $return;
        // process transforms
        $this->processTransforms();
        // Encrypt the encrypted fields
        $this->prepareEncryptedFieldsForDB();
        // Add in the default fields
        $this->prepareDefaultFieldsForDB();
        // Add in the preset fields
        $this->preparePresetFieldsForDB();
        // Add missing or implied fields
        $this->prepareImpliedFieldsForDB();
        // fix encryptable for fake fields
        $this->applyEncryptableOnFakeFields();
        // Run the backpack store
        ClassHelper::callIfExists($this, 'before_store');
        try {
            $result = $this->backpackStore();
        } catch (\Exception $e) {
            ClassHelper::callIfExists($this, 'after_store', null, false, $e);
            throw $e;
        }
        return ClassHelper::callIfExists($this, 'after_store', $result, $this->isSuccessful($result), null)
            ?? $result;
    }

}
