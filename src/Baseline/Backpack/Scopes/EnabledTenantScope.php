<?php
/**
 * Created by PhpStorm.
 * User: darren
 * Date: 2019/12/04
 * Time: 13:49
 */

namespace Baseline\Backpack\Scopes;

use Baseline\Backpack\Helpers\TenantHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class EnabledTenantScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where($model->getTable().'.'.config('tenant.foreign_key'), '=', TenantHelper::currentTenantId());
        $builder->whereHas(config('tenant.relation'), function($builder) {
            $builder->where('enabled',1);
        });

    }
}
