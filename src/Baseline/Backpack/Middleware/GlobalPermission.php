<?php

namespace Baseline\Backpack\Middleware;

use Baseline\Backpack\Helpers\TenantHelper;
use Closure;
use Illuminate\Support\Facades\Log;

class GlobalPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!TenantHelper::amISuperTenant()) {
            Log::error("Cannot access global permission, super_tenant is ".config('tenant.super_tenant').", current tenant is ".TenantHelper::currentTenantId());
            abort(403);
        }
        return $next($request);
    }
}
