<?php

namespace Baseline\Backpack\Middleware;

use Baseline\Backpack\Helpers\TenantHelper;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class CheckActivityLogAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(str_starts_with(Route::current()->uri(),config('backpack.base.route_prefix', 'admin')."/activity-log") && !TenantHelper::amISuperTenant()) {
            Log::error("Cannot access activity-log, super_tenant is ".config('tenant.super_tenant').", current tenant is ".TenantHelper::currentTenantId());
            abort(403);
        }
        return $next($request);
    }
}
