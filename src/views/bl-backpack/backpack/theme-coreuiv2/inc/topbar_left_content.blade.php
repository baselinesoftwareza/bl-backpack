{{-- This file is used to store topbar (left) items --}}

<li class="nav-item">
    <a class="nav-link btn btn-outline search-button p-1" href="{{ backpack_url('overview') }}" role="button" aria-haspopup="true"
       aria-expanded="false" data-toggle="tooltip" title="Overview">
        <span class="la la-home"></span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link btn btn-outline search-button p-1" href="{{ backpack_url('alertboard') }}" role="button" aria-haspopup="true"
       aria-expanded="false" data-toggle="tooltip" title="Alert Board">
        <span class="la la-warning"></span>
    </a>
</li>
