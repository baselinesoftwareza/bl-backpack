target_field = $("[{!! \Illuminate\Support\Str::contains($name,'=') ? $name : "name='$name'" !!}]");

action = {!! json_encode($action, JSON_HEX_TAG) !!}
fm_process_disabled(target_field,action);
fm_process_value(target_field,action);
fm_process_checked(target_field,action);
fm_process_parent_visible(target_field,action);
fm_process_grand_parent_visible(target_field,action);
fm_process_function(target_field,action);
