
// remove the responsible datatable click nonsense (we are overriding with our own popup)
$(this).find('tbody').off('click');
let row = $(this).find('tbody>tr').each(function() {
    let tr = $(this);
    let id = $(this).find('td span[record-id]').attr('record-id');
    let cells = $(this).find('td');
    // for every cell but the last one
    for(let i = 0; i < cells.length - 1; i++) {
        $(cells[i]).off('click');
        $(cells[i]).on('click',{!! $function !!});
        $(cells[i]).css('cursor', 'pointer');
    }
});
