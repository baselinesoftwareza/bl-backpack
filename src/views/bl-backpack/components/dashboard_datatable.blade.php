<table class="table table-striped table-hover m-0 dashboard-datatable" style="width: 100%"
       data-url="{{ $ddt['data_url'] ?? '' }}"
       id="{{ $ddt['id'] }}"
       data-fn-data="{{ $ddt['data_function'] ?? "" }}"
       data-refresh="{{ $ddt['refresh'] ?? "" }}"
>
    <thead>
    <tr>
        @foreach($ddt["columns"] ?? [] as $column)
            <th
                @foreach($column as $option => $value)
                    data-{{ \Illuminate\Support\Str::kebab($option) }}="{{ is_scalar($value) ? $value : json_encode($value) }}"
                @endforeach
            >{{ $column['label'] ?? "" }}
            </th>
        @endforeach
    </tr>
    </thead>
</table>

@push('after_scripts')
    <script>
        $(function () {
            $('#{{ $ddt['id'] }}').on('draw.dt', function () {
                initTooltips();
            })
        });
    </script>
@endpush

@push('after_styles')
    {{-- include select2 css --}}
    <link href="{{ asset('packages/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <style>
        .form-inline .select2-container {
            display: inline-block;
        }

        .select2-drop-active {
            border: none;
        }

        .select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices {
            border: none;
        }

        .select2-container-active .select2-choice {
            border: none;
            box-shadow: none;
        }

        .select2-container--bootstrap .select2-dropdown {
            margin-top: -2px;
            margin-left: -1px;
        }

        .select2-container--bootstrap {
            position: relative !important;
            top: 0px !important;
        }
    </style>
@endpush
