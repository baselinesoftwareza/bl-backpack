@extends('bl-backpack.layout.dashboard')

@section('content')

    <div class="row">

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Company Details</h3>
                </div>
                <table class="table card-table table-vcenter">
                    <tbody>
                    <tr>
                        <th>Company Code</th>
                        <td>{{ $entry->code }}</td>
                    </tr>
                    <tr>
                        <th>Company Name</th>
                        <td>{{ $entry->name }}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td>{!! str_replace("\n","<br/>",$entry->address) !!}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $entry->email }}</td>
                    </tr>
                    <tr>
                        <th>Phone Number</th>
                        <td>{{ $entry->telephone }}</td>
                    </tr>
                    </tbody>
                </table>
                <div class="card-footer">
                    <a class="btn btn-primary"
                       style="float: right"
                       href="{{ backpack_url('tenant',[ $entry->id, 'edit' ]) }}"><i class="la la-edit"></i> Edit</a>
                </div>
            </div>
        </div>

        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Other Things</h3>
                </div>
                <table class="table card-table table-vcenter">
                    <thead>
                    <tr>
                        <th>Attr 1</th>
                        <th>Attr 2</th>
                    </tr>
                    </thead>
                    <tbody>
{{--                    @foreach($entry->things as $thing)--}}
{{--                        <tr>--}}
{{--                            <td>{{ $thing->number }}</td>--}}
{{--                            <td>{{ $thing->name }}</td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
                    </tbody>
                </table>
                <div class="card-footer">
                    <a class="btn btn-primary"
                       style="float: right"
                       href="{{ backpack_url('thing') }}"><i class="la la-list"></i> List Things</a>
                </div>
            </div>
        </div>
    </div>
@endsection
