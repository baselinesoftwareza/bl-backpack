@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    @include('crud::fields.inc.translatable_icon')

        <input
            type="text"
            name="{{ $field['name'] }}"
            value="{{ old_empty_or_null($field['name'], '') ??  $field['value'] ?? $field['default'] ?? '' }}"
            @include('crud::fields.inc.attributes')
            data-toggle="basename"
            data-spacer-char="{{ $field['spacer_char'] ?? '-' }}"
            data-generate-from="{{ $field['generate_from'] ?? '' }}"
            data-no-dash="{{ ($field['nodash'] ?? false) ? 'true' : 'false' }}"
            data-max-length="{{ $field['maxlength'] ?? 'false' }}"
        >

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
@include('crud::fields.inc.wrapper_end')

@push('crud_fields_scripts')
    @loadOnce("/bl-backpack/js/basename_generator.js")
@endpush
