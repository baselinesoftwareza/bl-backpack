@php($value = old($field['name']) ?? $field['value'] ?? $field['default'] ?? $field['min'] ?? 0)
<!-- text input -->
@include('crud::fields.inc.wrapper_start')
<input type="hidden" id="slider-value-{{ $field['name'] }}" name="{{ $field['name'] }}" value="{{ $value }}"/>
<label style="width: 100%">{!! $field['label'] !!}</label><span id="amount-{{ $field['name'] }}"></span>
{{-- HINT --}}
@if (isset($field['hint']))
    <div style="float: right"><span class="help-block" style="margin-bottom: 5px; margin-top: 0px">{!! $field['hint'] !!}</span></div>
@endif
<div id="slider-{{ $field['name'] }}"></div>
@push('after_scripts')
    <script>

        var slider_function = {!! $field['display_function'] ?? 'function(value) { return value; }' !!}

        $( function() {
            $( "#slider-{{ $field['name'] }}" ).slider({
                value: {{ $value }},
                step: {{ $field['step'] ?? 'undefined' }},
                min: {{ $field['min'] ?? 0 }},
                max: {{ $field['max'] ?? 100 }},
                slide: function( event, ui ) {
                    $('input[name={{ $field['name'] }}]').val( ui.value );
                    $('#amount-{{ $field['name'] }}').text(slider_function(ui.value));
                }
            });
            $('#amount-{{ $field['name'] }}').text(slider_function('{{ $value }}'));
        } );
    </script>
@endpush
@include('crud::fields.inc.wrapper_end')

@push('crud_fields_styles')
    @loadOnce("/packages/jquery-ui-dist/jquery-ui.css")
@endpush

@push('crud_fields_scripts')
    @loadOnce("packages/jquery-ui-dist/jquery-ui.js")
@endpush
