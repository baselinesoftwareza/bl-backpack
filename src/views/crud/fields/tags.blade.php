@include('crud::fields.inc.wrapper_start')
@php($current = old($field['name']) ? old($field['name']) : ($field['value'] ?? []))
<label>{!! $field['label'] !!}</label>
@if(isset($field['prefix']) || isset($field['suffix']))
    <div class="input-group"> @endif
        @if(isset($field['prefix']))
            <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div>
        @endif
        <select data-type="tags" name="{{ $field['name'] }}[]" class="form-control" multiple="multiple"
                data-seperators="{{ $field['seperators'] ?? ', ' }}">
            @if($current ?? [])
                @foreach($current as $tag)
                    <option selected="selected">{{ $tag }}</option>
                @endforeach
            @endif
            @foreach(array_diff($field['tags'] ?? [],$current ?? []) as $tag)
                <option>{{ $tag }}</option>
            @endforeach
        </select>
        @if(isset($field['suffix']))
            <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div>
        @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div>
@endif
{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
@include('crud::fields.inc.wrapper_end')


@push('crud_fields_scripts')
    @loadOnce('tagsCode')
    <script>
        $(function () {
            $("select[data-type=tags]").each(function () {
                let delims = $(this).data('seperators');
                $(this).select2({
                    tags: true,
                    tokenSeparators: delims.split(''),
                    width: '100%',
                    // createTag: function (params) {
                    //     var term = $.trim(params.term);
                    //
                    //     if (term === '') {
                    //         return null;
                    //     }
                    //
                    //     return {
                    //         id: term,
                    //         text: term,
                    //         newTag: true // add additional parameters
                    //     }
                    // }
                });
            });
        });
        // This if for field management support, to ensure we can easily adjust tags when using field management
        // dataList needs to be of the form ['tag1','tag2',...]
        function fm_setTags(name, dataList = [], only_if_empty = false) {
            let target = $(`[name='${name}']`);
            if (target.length > 0 && (!only_if_empty || !target.val().length)) {
                target?.empty();
                for (option of dataList) {
                    target?.append(new Option(option, option));
                }
                target?.val(dataList).trigger('change');
            }
            // target?.select2('data', dataList);
        }
    </script>
    @endLoadOnce
@endpush
