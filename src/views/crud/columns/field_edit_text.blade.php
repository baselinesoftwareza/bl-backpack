@php
    $value = $column['value'] ?? data_get($entry, $column['name']);
    $settings = ($column['settings'] ?? null) ? (is_callable($column['settings']) ? ($column['settings'])($entry) : $column['settings']) : [];
    $on_update = ($column['on_update'] ?? []);
@endphp
<div class="field_edit_text field_edit" style="display: flex; align-items: center"
     data-nullable="{{ $column['nullable'] ?? true }}"
     data-field="{{ $column['name'] }}"
     data-field-label="{{ $column['label'] }}"
     data-field-value="{{ $value }}"
     data-field-limit="{{ $column['limit'] ?? 32 }}"
     data-field-escaped="{{ $column['escaped'] ?? true }}"
     data-entry-id="{{ $entry->getKey() }}"
     data-update-field-value="{{ $on_update['field_value'] ?? ("{".$column['name']."}") }}"
     data-tooltip="{{ $on_update['tooltip'] ?? null }}"
>
    @include('crud::columns.text')
    <button class="btn btn-sm"
       data-toggle="tooltip" title="Edit">
        <span class="data-field-null" style="display: {{ $entry->{$column['name']} ? 'none' : 'unset' }}">{{ $settings['null_text_label'] ?? null }}</span>
        <span><i class="la la-edit"></i></span>
    </button>
    <input type="text" name="text" style="display: none; width: 90%" />
    <div class="pl-1">
        <div class="update_spinner spinner-grow spinner-grow-sm" style="display: none"><span class="sr-only">Updating...</span></div>
        <div class="update_success" style="display: none; color: rgb(54 165 54)"><i class="la la-check"></i></div>
        <div class="update_error" style="display: none; color: #FF0000"><i class="la la-times"></i></div>
    </div>
</div>
