@php

    $value = $column['value'] ?? $entry->{$column['name']};
    if(is_callable($value)) {
        $value = $value($entry);
    }

@endphp
{{-- regular object attribute --}}
<span style="align-content: center; text-align: center">
    <form action="/{{ $crud->route }}/{{ $entry->id }}/toggle" style="">
        <input type="hidden" name="attribute" value="{{ $column['name'] }}"/>
        @if(isset($column['default_checkbox']) && $column['default_checkbox'])
            <input type="hidden" name="default_checkbox" value="1"/>
        @endif
        @if(\Baseline\Backpack\Helpers\UIHelper::isBootstrap5())
            <div class="form-check form-switch">
              <input class="form-check-input" type="checkbox"
                     name="{{ $column['name'] }}" {{ $value ? 'checked' : '' }} onchange="submit()"
                     data-toggle="tooltip"
                     @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $value)
                         disabled
                     @endif
                     @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $value)
                         title="Click to enable"
                     @else
                         title="Click to disable"
                  @endif
              >
            </div>
        @else
            <label class="switch switch-sm switch-label switch-outline-success" style="margin: 3px">
            <input class="switch-input" type="checkbox"
                   name="{{ $column['name'] }}" {{ $value ? 'checked' : '' }} onchange="submit()"
                   data-toggle="tooltip"
                   @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $value)
                       disabled
                  @endif
            ><span class="switch-slider" data-checked="✓" data-unchecked="✕"
                   @if(isset($column['default_checkbox']) && $column['default_checkbox'] && $value)
                       title="Click to enable"
                   @else
                       title="Click to disable"
                  @endif
            ></span>
        </label>
        @endif
    </form>
</span>
