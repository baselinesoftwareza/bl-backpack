@php
    $value = ($column['value'] ?? null) ? (is_callable($column['value']) ? ($column['value'])($entry) : $column['value']) : $entry->{$column['name']};
    $settings = ($column['settings'] ?? null) ? (is_callable($column['settings']) ? ($column['settings'])($entry) : $column['settings']) : [];
    $on_update = ($column['on_update'] ?? []);
    $dropdown_options = is_array($column['dropdown_options'] ?? null) ? collect($column['dropdown_options']) : ($column['dropdown_options'] ?? '');
@endphp
<div class="field_edit field_edit_select2" style="display: flex"
     data-nullable="{{ $column['nullable'] ?? true }}"
     data-field="{{ $column['name'] }}"
     data-field-label="{{ $column['label'] }}"
     data-field-id="{{ $entry->{$column['name']} }}"
     data-entry-id="{{ $entry->id }}"
     data-field-escaped="{{ $column['escaped'] ?? true }}"
     data-options="{{ $dropdown_options ?? '' }}"
     data-update-field-value="{{ $on_update['field_value'] ?? "{".$column['name']."}" }}"
     data-update-field-id="{{ $on_update['field_id'] ?? "{".$column['name']."}" }}"
     data-url="{{ $on_update['url'] ?? null }}"
     data-tooltip="{{ $on_update['tooltip'] ?? null }}"
>
    @if(isset($settings['url']))
        <a href="{{ $settings['url'] }}" class="data-field-display"
           @if(isset($settings['tooltip']))
               data-toggle="tooltip" title="{{ $settings['tooltip'] }}"
           @endif
        >
            @if($column['escaped'] ?? true)
                {{ $value }}
            @else
                {!! $value !!}
            @endif
        </a>
    @else
        <span class="data-field-display"
          @if(isset($settings['tooltip']))
              data-toggle="tooltip" title="{{ $settings['tooltip'] }}"
          @endif
        >
            @if($column['escaped'] ?? true)
                {{ $value }}
            @else
                {!! $value !!}
            @endif
        </span>
    @endif
    <a href="#" class="nav-link dropdown-toggle p-0"
       data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span class="data-field-null" style="display: {{ $entry->{$column['name']} ? 'none' : 'unset' }}">{{ $settings['null_select_label'] ?? null }}</span>
    </a>
    <div class="dropdown-menu">
        <div class="form-group backpack-filter mb-0">
            <select class="form-control input-sm select2">
            </select>
        </div>
    </div>
    <div class="pl-1">
        <div class="update_spinner spinner-grow spinner-grow-sm" style="display: none"><span class="sr-only">Updating...</span></div>
        <div class="update_success" style="display: none; color: rgb(54 165 54)"><i class="la la-check"></i></div>
        <div class="update_error" style="display: none; color: #FF0000"><i class="la la-times"></i></div>
    </div>
</div>
