@php($parent = (\Illuminate\Support\Facades\Route::current()->parameterNames())[0])
<a
    href="{{ url(\Baseline\Backpack\Helpers\RouteHelper::getParsedControllerRoute($parent,"{".$parent."}/dashboard")) }}"
    class="btn btn-info ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="la la-backward"></i>
            Back To Dashboard
        </span></a>
