<div class="modal fade" id="modal-popup" tab-index="-1" role="dialog" aria-labelledby="popupModal" aria-hidden="true">
    <div class="modal-dialog {{ $modal_class ?? 'modal-info' }}" role="document">
        <div class="modal-content">
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
<script>
    $('#modal-popup')
        .on('show.bs.modal', function (e) {
            let caller = $(e.relatedTarget);
            $("#modal-popup div.modal-content").load("/{{ $crud->route }}/" + caller.attr('data-id') + "/popup");
        })
        .on('hide.bs.modal', function (e) {
            $("#modal-popup div.modal-content").empty();
        });

</script>
