@if ($crud->hasAccess('popup'))
    <button type="button" class="btn btn-link btn-sm" data-toggle="modal" data-target="#modal-popup"
            data-id="{{ $entry->id }}"
    > <i class="la la-eye"></i> &nbsp;&nbsp;View</button>
    @include_once('backpack::crud.popup.popup_modal',[ 'modal_class' => $crud->get('popup.modal_class','') ])
@endif
